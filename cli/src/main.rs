pub mod commands;
mod file;
pub use crate::file::{load_config_file, save_config_file};

pub use commands::Command;

fn main() {
   match Command::from_args() {
      Command::Init(cmd) => println!("{:?}", cmd.execute()),
      Command::InitNode(cmd) => println!("{:?}", cmd.execute()),
      Command::Start(cmd) => println!("{:?}", cmd.execute()),
      Command::Keys(cmd) => println!("{:?}", cmd.execute()),
      Command::Tx(cmd) => println!("{:?}", cmd.execute()),
      Command::Query(cmd) => println!("{:?}", cmd.execute()),
   }
}

#[cfg(test)]
#[path = "./cli_test.rs"]
mod cli_test;
