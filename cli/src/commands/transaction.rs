use error::error::MgError;
use structopt::StructOpt;

pub mod send_tx;
use crate::commands::transaction::send_tx::SendTxCmd;

/// Key utilities for the cli.
#[derive(Debug, StructOpt)]
pub enum TxCmd {
    SendTx(SendTxCmd),
}

impl TxCmd {
    pub fn execute(&self) -> Result<(), MgError> {
        match self {
            TxCmd::SendTx(cmd) => cmd.execute(),
        }
    }
}
