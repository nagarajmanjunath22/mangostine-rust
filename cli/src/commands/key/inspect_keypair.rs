extern crate types;
pub use crypto;
use crypto::keypair::keypair_from_seed;
use error::error::MgError;
use structopt::StructOpt;
use subtle_encoding::hex;
use types::address::address_from_public_key;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "inspect-key",
    about = "Get a random key-pair by passing menomics, save it to \
			 file or print it to stdout and print its keypair to stderr"
)]

pub struct InspectKeyCmd {
    /// mnemonics which contains 12 words or 24 words to generate the keypair
    #[structopt(long)]
    mnemonic: Option<String>,
}

impl InspectKeyCmd {
    /// Run the command
    pub fn execute(&self) -> Result<(), MgError> {
        let seeds = (self.mnemonic.as_ref()).unwrap().as_bytes();
        let kp = keypair_from_seed(seeds).unwrap();
        let addr = address_from_public_key(*kp.public());
        let sceret = hex::encode(kp.secret().as_bytes());
        let public = hex::encode(kp.public().as_bytes());
        let address = hex::encode(addr.as_bytes());
        println!("Sceret Key {:?}", String::from_utf8(sceret).unwrap());
        println!("Publick Key {:?}", String::from_utf8(public).unwrap());
        println!("Address Key {:?}", String::from_utf8(address).unwrap());
        Ok(())
    }
}
