use crate::commands::init_node;
use crate::file::save_json_file;
pub use crypto;
use crypto::keypair::Keypair;
use error::error::MgError;
use init_node::get_input;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use std::process;
use structopt::StructOpt;
use subtle_encoding::hex;
use types::address::Address;
use types::address::address_from_public_key;

pub const PUBLIC_KEYS_FILE_NAME: &str = "keys.json";
#[derive(Debug, StructOpt)]
#[structopt(
    name = "generate",
    about = "Generate a random key-pair, save it to \
			 file or print it to stdout and print its keypair to stderr"
)]

pub struct GenerateCmd {
    /// Name of file to save secret key to.
    ///
    /// If not given, the secret key is printed to stdout.
    #[structopt(short = "o", default_value = "")]
    pub output_dir: String,
}

impl Default for GenerateCmd {
    fn default() -> Self {
        Self {
            output_dir: dirs::home_dir().unwrap().to_str().unwrap().to_owned()
                + "/mangostine/Keypair",
        }
    }
}
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Keys {
    pub mnemonic: String,
    pub secret_key: String,
    pub pub_key: String,
    pub address: Address,
}

impl Keys {
    pub fn new(mnemonic: String, secret_key: String, pub_key: String, address: Address) -> Keys {
        Keys {
            mnemonic: mnemonic,
            secret_key: secret_key,
            pub_key: pub_key,
            address: address,
        }
    }
}

impl GenerateCmd {
    /// Run the command
    pub fn execute(&self) -> Result<(), MgError> {
        let mut key_path = PathBuf::new();
        let mut path = self.output_dir.clone();
        let cp = GenerateCmd::default().output_dir;
        if path.is_empty() {
            path = cp;
        }
        let node_input = get_input("Please enter Number of accounts to be created").unwrap();
        if node_input <= 0 {
            println!("Number of node must be greater than 0",);
            process::exit(1);
        }
        key_path = key_path
            .join(&path)
            .join("Keypair")
            .join(PUBLIC_KEYS_FILE_NAME);
        let mut acc_key: Vec<Keys> = Vec::new();
        for _ in 0..node_input {
            let (kp, mnemonic) = Keypair::generate_with_seed();
            let addr = address_from_public_key(*kp.public());
            let sceret = hex::encode(kp.secret().as_bytes());
            let public = hex::encode(kp.public().as_bytes());
            let keys = Keys::new(
                mnemonic,
                String::from_utf8(sceret).unwrap(),
                String::from_utf8(public).unwrap(),
                addr,
            );
            acc_key.push(keys);
        }
        save_json_file(&acc_key, &key_path).expect("key file already exist");
        Ok(())
    }
}
