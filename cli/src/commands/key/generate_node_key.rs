use error::error::MgError;
use std::path::PathBuf;
use structopt::StructOpt;

/// The `generate-node-key` command
#[derive(Debug, StructOpt)]
#[structopt(
	name = "generate-node-key",
	about = "Generate a random node libp2p key, save it to \
			 file or print it to stdout and print its peer ID to stderr"
)]
pub struct GenerateNodeKeyCmd {
	#[structopt(long)]
	file: Option<PathBuf>,
}

impl GenerateNodeKeyCmd {
	/// Run the command
	pub fn execute(&self) -> Result<(), MgError> {
		Ok(())
	}
}
