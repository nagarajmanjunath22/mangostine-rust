use error::error::MgError;
use structopt::StructOpt;

pub mod acc_query;
use crate::commands::query::acc_query::AccQueryCmd;

/// query account for the cli.
#[derive(Debug, StructOpt)]
pub enum QueryCmd {
    Account(AccQueryCmd),
}

impl QueryCmd {
    pub fn execute(&self) -> Result<(), MgError> {
        match self {
            QueryCmd::Account(cmd) => cmd.execute(),
        }
    }
}
