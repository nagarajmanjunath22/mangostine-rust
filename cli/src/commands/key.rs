use error::error::MgError;
use structopt::StructOpt;

pub mod generate_keypair;
pub mod generate_node_key;
pub mod inspect_keypair;
use crate::commands::key::generate_keypair::GenerateCmd;
use crate::commands::key::generate_node_key::GenerateNodeKeyCmd;
use crate::commands::key::inspect_keypair::InspectKeyCmd;

/// Key utilities for the cli.
#[derive(Debug, StructOpt)]
pub enum KeyCmd {
	/// Generate a random node libp2p key, save it to file or print it to stdout
	/// and print its peer ID to stderr.
	GenerateNodeKey(GenerateNodeKeyCmd),
	/// Generate a random account
	Generate(GenerateCmd),

	InspectKey(InspectKeyCmd),
}

impl KeyCmd {
	pub fn execute(&self) -> Result<(), MgError> {
		match self {
			KeyCmd::GenerateNodeKey(cmd) => cmd.execute(),
			KeyCmd::Generate(cmd) => cmd.execute(),
			KeyCmd::InspectKey(cmd) => cmd.execute(),
		}
	}
}
