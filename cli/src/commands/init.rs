extern crate types;
use crate::commands::key::generate_keypair::Keys;
use crate::file::{save_config_file, save_genesis_file, save_json_file};
use config::config::Config;
use crypto::keypair::Keypair;
use error::error::MgError;
use genesis::genesis;
use std::net::SocketAddr;
use std::path::PathBuf;
use structopt::StructOpt;
use subtle_encoding::hex;
use types::account::Account;
use types::address::address_from_public_key;

pub const PUBLIC_CONFIG_FILE_NAME: &str = "config.toml";
pub const PUBLIC_GENESIS_FILE_NAME: &str = "genesis.json";
pub const PUBLIC_KEYS_FILE_NAME: &str = "keys.json";

/// The `generate-node-key` command
#[derive(Debug, StructOpt)]
#[structopt(
    name = "init",
    about = "init command Initialized the config file in root dir"
)]

pub struct InitCmd {
    #[structopt(short = "o", default_value = "")]
    pub output_dir: String,
    #[structopt(short, long, default_value = "127.0.0.1:5333")]
    pub peer_address: String,
    #[structopt(long, short = "l", default_value = "127.0.0.1:6333")]
    pub listen_address: SocketAddr,
}

impl Default for InitCmd {
    fn default() -> Self {
        Self {
            output_dir: dirs::home_dir().unwrap().to_str().unwrap().to_owned() + "/mangostine",
            peer_address: String::from("127.0.0.1:5333"),
            listen_address: ("127.0.0.1:6333")
                .parse()
                .expect("Unable to parse socket address"),
        }
    }
}

impl InitCmd {
    /// Run the command
    pub fn execute(&self) -> Result<(), MgError> {
        let mut config_path = PathBuf::new();
        let mut genesis_path = PathBuf::new();
        let mut key_path = PathBuf::new();
        let mut path = self.output_dir.clone();
        let cp = InitCmd::default().output_dir;
        if path.is_empty() {
            path = cp;
        }
        config_path = config_path.join(&path).join(PUBLIC_CONFIG_FILE_NAME);
        genesis_path = genesis_path.join(&path).join(PUBLIC_GENESIS_FILE_NAME);
        key_path = key_path
            .join(&path)
            .join("Keypair")
            .join(PUBLIC_KEYS_FILE_NAME);
        let node_config = Config::default();
        let mut genesis = genesis::Genesis::default();
        let mut acc_key: Vec<Keys> = Vec::new();
        for _ in 1..10 {
            let (kp, mnemonic) = Keypair::generate_with_seed();
            println!("mnemonic {:?}", mnemonic);
            let addr = address_from_public_key(*kp.public());
            let sceret = hex::encode(kp.secret().as_bytes());
            let public = hex::encode(kp.public().as_bytes());
            let keys = Keys::new(
                mnemonic,
                String::from_utf8(sceret).unwrap(),
                String::from_utf8(public).unwrap(),
                addr.clone(),
            );
            let acc = Account::new(&addr, 150, 0, false);
            genesis.accounts.push(acc);
            acc_key.push(keys);
        }
        save_config_file(&node_config, &config_path).expect("config file already exist");
        save_genesis_file(&genesis, &genesis_path).expect("genesis file already exist");
        save_json_file(&acc_key, &key_path).expect("key file already exist");
        Ok(())
    }
}
