extern crate types;
use crate::commands::key::generate_keypair::Keys;
use crate::file::load_json_file;
pub use crypto;
use crypto::keypair::{keypair_from_seed, Keypair};
use ed25519_dalek::PublicKey;
use error::error::MgError;
use grpc::mangostine_v0::{rpc_client, RawTransaction};
use log::info;
use structopt::StructOpt;
use tokio::runtime::Runtime;
use types::address::Address;
use types::transaction::Transaction;
#[derive(Debug, StructOpt)]
#[structopt(
    name = "send-tx",
    about = "send the transaction, {sender}, {receiver}, {amount}, {sequence}"
)]

pub struct SendTxCmd {
    #[structopt(long = "sender", short = "from")]
    sender: Option<Address>,
    #[structopt(long = "receiver", short = "to")]
    receiver: Option<Address>,
    #[structopt(long = "amount", short = "amt")]
    amount: Option<u64>,
    #[structopt(long = "sequence", short = "s")]
    sequence: Option<u64>,
    #[structopt(short = "o", default_value = "")]
    pub output_dir: String,
    #[structopt(long = "http", short = "p", default_value = "localhost:16657")]
    pub http: String,
    #[structopt(long = "method", short = "m", default_value = "send_raw_tx")]
    pub method: String,
}

impl SendTxCmd {
    /// Run the command
    pub fn execute(&self) -> Result<(), MgError> {
        if let Err(e) = self.verify_basic() {
            return Err(MgError::InvalidTx(e.to_string().to_owned()));
        }
        // get the account from key json including mnemonics
        let kp = match self.get_account() {
            Ok(account) => Ok(account),
            Err(e) => Err(MgError::InvalidTx(e.to_string().to_owned())),
        };

        //create transaction using sender receiver address with amount
        let mut tx = Transaction::new(
            self.sender.clone().unwrap(),
            self.receiver.clone().unwrap(),
            self.amount.unwrap(),
            self.sequence.unwrap(),
        );

        // get the key_pair from mnemonics
        let mn = keypair_from_seed(kp.unwrap().mnemonic.as_bytes());
        let key_pair = Keypair::from_bytes(&mn.unwrap().to_bytes());
        let keys = key_pair.unwrap();

        let pub_key = PublicKey::from_bytes(keys.public().as_ref()).unwrap();
        let sign_value = tx.sign_bytes();
        let signature = keys.try_sign(&sign_value).unwrap();
        tx.public_key = Some(pub_key);
        tx.signature = Some(signature);
        let tx_bytes = tx.encode();
        let mut rt = Runtime::new().unwrap();
        match rt.block_on(call_send_raw_tx(tx_bytes, self.http.clone())) {
            Ok(_) => info!("Done"),
            Err(e) => return Err(MgError::InvalidTx(e.to_string())),
        };
        Ok(())
    }

    /// Verify the basic validation
    pub fn verify_basic(&self) -> Result<(), MgError> {
        let sender = (self.sender.as_ref()).unwrap().as_bytes();
        let receiver = (self.receiver.as_ref()).unwrap().as_bytes();
        let amount = (self.amount.as_ref()).unwrap();
        if sender.len() == 0 && sender.len() < 19 {
            return Err(MgError::InvalidTx(
                "The sender address must contains value of 20 bytes".to_owned(),
            ));
        }
        if receiver.len() == 0 && receiver.len() < 19 {
            return Err(MgError::InvalidTx(
                "The receiver address must contains value of 20 bytes".to_owned(),
            ));
        }
        if amount <= &u64::MIN {
            return Err(MgError::InvalidTx("The amount cannot be empty".to_owned()));
        }
        Ok(())
    }

    // load the accounts from Key.json file
    pub fn get_account(&self) -> Result<Keys, MgError> {
        let path: String =
            dirs::home_dir().unwrap().to_str().unwrap().to_owned() + "/mangostine/Keypair";

        let values = match load_json_file(path + "/keys.json") {
            Ok(account) => Ok(account),
            Err(e) => Err(MgError::InvalidTx(format!(
                "The key.json is invalid: {}",
                e
            ))),
        };
        let accounts: Vec<Keys> = values.unwrap();
        let sender = self.sender.clone().unwrap();
        for acc in accounts {
            if acc.address == sender {
                return Ok(acc);
            };
        }

        return Err(MgError::InvalidTx(
            "The sender address is invalid".to_owned(),
        ));
    }
}
// call the raw transaction
async fn call_send_raw_tx(tx: Vec<u8>, http: String) -> Result<(), Box<dyn std::error::Error>> {
    let mut client = rpc_client::RpcClient::connect(http).await?;
    let request = tonic::Request::new(RawTransaction { raw_tx: tx });
    let response = client.send_raw_tx(request).await?;
    println!("RESPONSE={:?}", response);
    Ok(())
}
