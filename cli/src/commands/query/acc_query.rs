extern crate types;
use error::error::MgError;
use grpc::mangostine_v0::{rpc_client, AccountRequest};
use log::info;
use structopt::StructOpt;
use tokio::runtime::Runtime;
use types::address::Address;
#[derive(Debug, StructOpt)]
#[structopt(name = "account", about = "get the account details")]

pub struct AccQueryCmd {
    #[structopt(long = "address", short = "addr")]
    address: Option<Address>,
    #[structopt(short = "o", default_value = "")]
    pub output_dir: String,
    #[structopt(long = "http", short = "p", default_value = "localhost:16657")]
    pub http: String,
    #[structopt(long = "method", short = "m", default_value = "get_account")]
    pub method: String,
}

impl AccQueryCmd {
    pub fn execute(&self) -> Result<(), MgError> {
        if self.address.is_none() {
            return Err(MgError::InvalidAccount("Address invalid".to_owned()));
        }
        let mut rt = Runtime::new().unwrap();
        match rt.block_on(query_account(
            self.address.clone().unwrap().to_string(),
            self.http.clone(),
        )) {
            Ok(_) => info!("Done"),
            Err(e) => return Err(MgError::InvalidAccount(e.to_string())),
        };
        Ok(())
    }
}

//query the account from Db
async fn query_account(addr: String, http: String) -> Result<(), Box<dyn std::error::Error>> {
    let mut client = rpc_client::RpcClient::connect(http).await?;
    println!("the adress in cli {:?}", addr);
    let request = tonic::Request::new(AccountRequest { address: addr });
    let response = client.get_account(request).await?;
    println!("RESPONSE={:?}", response);
    Ok(())
}
