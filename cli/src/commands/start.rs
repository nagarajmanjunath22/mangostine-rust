use crate::file::{load_config_file, load_genesis_file};
use async_std::task;
use config::config::Config;
use error::error::MgError;
use genesis::genesis::Genesis;
use grpc::Service;
use log::LevelFilter;
use mempool::{AccountStoreProvider, Mempool};
use network::Network;
pub use persistence;
use persistence::account_store::AccountStore;
use persistence::block_store::BlockStore;
use simple_logger::SimpleLogger;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, Mutex};
use std::thread;
use structopt::StructOpt;

/// The `generate-node-key` command
#[derive(Debug, StructOpt)]
#[structopt(name = "start", about = "run the node")]
pub struct StartCmd {
    #[structopt(short = "o", default_value = "")]
    output_dir: String,
}

impl Default for StartCmd {
    fn default() -> Self {
        Self {
            output_dir: dirs::home_dir().unwrap().to_str().unwrap().to_owned() + "/mangostine",
        }
    }
}

impl StartCmd {
    /// Run the command
    pub fn execute(&self) -> Result<(), MgError> {
        let dir = self.output_dir.clone();
        let running = Arc::new(AtomicBool::new(true));
        let r = running.clone();
        ctrlc::set_handler(move || {
            r.store(false, Ordering::SeqCst);
        })
        .expect("Error setting Ctrl-C handler");

        SimpleLogger::new()
            .with_module_level("network::network", LevelFilter::Info)
            .with_module_level("libp2p_tcp", LevelFilter::Info)
            .with_module_level("network::behaviour", LevelFilter::Info)
            .with_level(LevelFilter::Info)
            .init()
            .expect("unable to start logger");

        //load the configuration file
        let config: Config = load_config_file(dir.clone() + "/config.toml").unwrap();

        //load the genesis file
        let genesis: Genesis = load_genesis_file(dir.clone() + "/genesis.json").unwrap();

        // init block_store.
        let _block_store = BlockStore::open(&StartCmd::default().output_dir);

        // init account_store.
        let account_store = AccountStore::open(&StartCmd::default().output_dir);
        //save accounts to db loaded from genesis
        for account in genesis.accounts {
            account_store.create_account(account);
        }

        let node_id = 0;
        let mut network = Network::new(config.network, node_id).ok().unwrap();
        let account_provider = AccountStoreProvider::new(account_store);
        let mempool = Mempool::new(config.mempool, account_provider).ok().unwrap();
        let mempool_guard = Arc::new(Mutex::new(mempool));
        let grpc_service = Service::new(config.grpc, mempool_guard.clone());

        let mempool_service = mempool::Service::new(mempool_guard, &mut network)
            .ok()
            .unwrap();
        let network_task = task::spawn(async {
            network.run().await;
        });
        let mempool_task = task::spawn(async {
            mempool_service.start().await;
        });

        // start grpc server.
        let grpc_server_task = task::spawn(async {
            if let Err(e) = grpc_service.start_server().await {
                panic!("Mangostine gRPC server start failed. {}", e)
            }
        });

        while running.load(Ordering::SeqCst) {
            thread::sleep(std::time::Duration::from_secs(1));
        }

        println!("Exiting...");

        // TODO: fix me!
        // TODO: gRPC server foesn't shutdown properly. Fix it later and make sure all tasks are shutdown properly
        let _handle = task::spawn(async {
            network_task.cancel().await;
            mempool_task.cancel().await;
            grpc_server_task.cancel().await;
        });

        // futures::executor::block_on(handle);

        Ok(())
    }
}
