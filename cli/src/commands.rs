use dirs::home_dir;
use error::error::MgError;
use std::path::PathBuf;
use structopt::StructOpt;

pub mod init;
pub mod init_node;
pub mod key;
pub mod query;
pub mod start;
pub mod transaction;

use crate::commands::init::InitCmd;
use crate::commands::init_node::InitNodeCmd;
use crate::commands::key::KeyCmd;
use crate::commands::query::QueryCmd;
use crate::commands::start::StartCmd;
use crate::commands::transaction::TxCmd;

pub trait MangostineCommand {
    /// Returns the result of the command execution.
    fn execute(self) -> Result<(), MgError>;
}

#[derive(Debug, StructOpt)]
pub enum Command {
    /// Key management cli utilities
    #[structopt(name = "key")]
    Keys(KeyCmd),
    ///Initialize the config and node keypair
    #[structopt(name = "init")]
    Init(InitCmd),
    ///Start the node using config and genesis file
    #[structopt(name = "start")]
    Start(StartCmd),

    ///Initialize multiple Node with config and genesis file
    #[structopt(name = "init-node")]
    InitNode(InitNodeCmd),

    ///send the transactions
    #[structopt(name = "tx")]
    Tx(TxCmd),

    ///send the transactions
    #[structopt(name = "query")]
    Query(QueryCmd),
}

impl Command {
    /// Wrapper around `StructOpt::from_args` method.
    pub fn from_args() -> Self {
        <Self as StructOpt>::from_args()
    }

    pub fn get_homedir() -> Option<PathBuf> {
        let path = home_dir();
        return path;
    }
}

impl MangostineCommand for Command {
    fn execute(self) -> Result<(), MgError> {
        match self {
            Self::Keys(command) => command.execute(),
            Self::Init(command) => command.execute(),
            Self::InitNode(command) => command.execute(),
            Self::Start(command) => command.execute(),
            Self::Tx(command) => command.execute(),
            Self::Query(command) => command.execute(),
        }
    }
}
