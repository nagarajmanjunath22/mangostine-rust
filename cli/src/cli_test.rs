#[cfg(test)]
mod tests {
    use mangostine_cli::commands::init::InitCmd;
    use mangostine_cli::commands::init_node::InitNodeCmd;
    use mangostine_cli::commands::key::generate_keypair::GenerateCmd;
    use mangostine_cli::commands::key::generate_node_key::GenerateNodeKeyCmd;
    use mangostine_cli::commands::key::inspect_keypair::InspectKeyCmd;
    use mangostine_cli::commands::start::StartCmd;
    use bip39::{Language, Mnemonic, MnemonicType};
    use structopt::StructOpt;
    use mangostine_cli::commands::transaction::send_tx::SendTxCmd;
    use mangostine_cli::commands::query::acc_query::AccQueryCmd;

    //cli command testing
    //
    /// cmd init which take default home directory.
    #[test]
    fn cli_init_cmd() {
        let generate = InitCmd::from_iter(&["init"]);
        assert!(generate.execute().is_ok())
    }
    /// cmd init with providing the home directory.
    #[test]
    fn cli_initcmd() {
        let out_dir = InitCmd::default().output_dir;
        let generate = InitCmd::from_iter(&["init", "-o", &out_dir]);
        assert!(generate.execute().is_ok());
    }

    /// cmd key generate, which will create the random account with 12 words mnemonics.
    #[test]
    fn cli_keypair_cmd() {
        let generate = GenerateCmd::from_iter(&["generate"]);
        assert!(generate.execute().is_ok())
    }
    /// cmd key generate, which will create the node lipp2p keypair.
    #[test]
    fn cli_keynode_cmd() {
        let generate = GenerateNodeKeyCmd::from_iter(&["generate-node-key"]);
        assert!(generate.execute().is_ok())
    }
    /// check the mnemonics word length
    #[test]
    fn cli_menomics_length() {
        let mnemonic = Mnemonic::new(MnemonicType::Words12, Language::English);
        let phrase = mnemonic.phrase();
        assert_eq!(phrase.split(" ").count(), 12);
    }
    /// check the entrophy word length
    #[test]
    fn cli_entropy_length() {
        let entropy = &[
            0x33, 0xE4, 0x6B, 0xB1, 0x3A, 0x74, 0x6E, 0xA4, 0x1C, 0xDD, 0xE4, 0x5C, 0x90, 0x84,
            0x6A, 0x79,
        ];
        let mnemonic = Mnemonic::from_entropy(entropy, Language::English).unwrap();

        assert_eq!(
            "crop cash unable insane eight faith inflict route frame loud box vibrant",
            mnemonic.phrase()
        );
        assert_eq!(
            "33E46BB13A746EA41CDDE45C90846A79",
            format!("{:X}", mnemonic)
        );
    }

    // cmd start, which will load the config and run mangostine.
    #[test]
    fn cli_start_dircmd() {
        let out_dir = InitCmd::default().output_dir;
        let generate = StartCmd::from_iter(&["start", "-o", &out_dir]);
        assert!(generate.execute().is_ok())
    }

    // cmd inspect-key, which will take the user provided mnemonics and generate the keypair.
    #[test]
    fn cli_inspect_key() {
        let inspect_key = InspectKeyCmd::from_iter(&[
            "inspect-key",
            "--mnemonic",
            "remember fiber forum demise paper uniform squirrel feel access exclude casual effort",
        ]);
        assert!(inspect_key.execute().is_ok())
    }

    // cmd InitNode, which will Initialized multiple node with config & genesis file in root dir
    //hints - 'cargo test cli_initnode_cmd -- --nocapture' : Run this command to ask for user-input
    #[test]
    fn cli_initnode_cmd() {

        let out_dir = InitNodeCmd::default().output_dir;
        let generate = InitNodeCmd::from_iter(&["init-node", "-o", &out_dir]);
        assert!(generate.execute().is_ok());

    }


    // cmd sendtx, send the transaction, {sender}, {receiver}, {amount}, {sequence}
    //hints - NEED to start blockchain server then only execute this process
    #[test]
    fn cli_test_sendtx_cmd() {

        let sendtx = SendTxCmd::from_iter(&[
            "push-tx",
            "--sender",
            "3CEB286D435CEC16EDB413A33EB0ADFA56A16BAC",
            "--receiver",
            "F86D59790E3708CCC4C0D9BC169D95CC8536E6AD",
            "--amount",
            "7",
            "--sequence",
            "0",
            "--http",
            "http://localhost:16657",
        ]);
        assert!(sendtx.execute().is_ok());

    }


    // cmd queryaccount, get the account details
    //hints - NO NEED to start blockchain server to execute this process
    #[test]
    fn cli_test_queryaccount_cmd() {

        let queryacc = AccQueryCmd::from_iter(&[
            "account",
            "--address",
            "3CEB286D435CEC16EDB413A33EB0ADFA56A16BAC",
            "--http",
            "http://localhost:16657",
        ]);
        assert!(queryacc.execute().is_ok());

    }


}
