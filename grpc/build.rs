use std::env;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let dir = env::current_dir().unwrap();
    let proto_path = dir.join("src/proto/v0/mangostine.proto");
    let out_dir = dir.join("src");

    tonic_build::configure()
        .format(true)
        .out_dir(&out_dir)
        .compile(&[proto_path], &[out_dir])?;
    Ok(())
}
