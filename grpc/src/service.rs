use crate::config::Config;
use crate::mangostine_v0::rpc_server::{Rpc, RpcServer};
use crate::mangostine_v0::{
    AccountRequest, AccountResponse, RawTransaction, Transaction, TransactionResponse,
};
use dirs;
use ed25519_dalek::{PublicKey, Signature};
use log::{error, info};
use mempool::{AccountStoreProvider, Mempool};
use persistence::account_store::AccountStore;
use serde_cbor;
use std::convert::TryFrom;
use std::str::FromStr;
use std::sync::{Arc, Mutex};
use tokio::runtime::Runtime;
use tonic::{transport::Server, Request, Response, Status};
use types::address::address_from_string;
use types::address::Address;
use types::transaction::{Recipient, Transaction as Tx};
pub struct Service {
    config: Config,
    mempool: Arc<Mutex<Mempool<AccountStoreProvider>>>,
}

// implementing rpc for service defined in .proto
#[tonic::async_trait]
impl Rpc for Service {
    // our rpc impelemented as function
    async fn send_tx(
        &self,
        request: Request<Transaction>,
    ) -> Result<Response<TransactionResponse>, Status> {
        let sender_addr = match Address::from_str(&request.get_ref().sender) {
            Ok(sender_addr) => sender_addr,
            Err(e) => return Err(Status::invalid_argument(e.to_string())),
        };
        let mut receiver: Vec<Recipient> = Vec::new();
        for r in &request.get_ref().receiver {
            let to_addr = match Address::from_str(&r.to) {
                Ok(to_addr) => to_addr,
                Err(e) => return Err(Status::invalid_argument(e.to_string())),
            };
            let send = Recipient {
                address: to_addr,
                amount: r.amount,
            };
            receiver.push(send);
        }

        let pub_key = match PublicKey::from_bytes(&request.get_ref().public_key) {
            Ok(pub_key) => pub_key,
            Err(e) => return Err(Status::invalid_argument(e.to_string())),
        };
        let signature = match Signature::try_from(&request.get_ref().signature[..]) {
            Ok(signature) => signature,
            Err(e) => return Err(Status::invalid_argument(e.to_string())),
        };

        let tx = Tx::new_multi_recipients(sender_addr, receiver, 0, Some(pub_key), Some(signature));
        let tx_hash = tx.hash();

        if let Err(e) = tx.verify_basic() {
            return Err(Status::invalid_argument(e.to_string()));
        }

        if let Err(e) = self.mempool.lock().unwrap().append_tx_and_broadcast(tx) {
            return Err(Status::internal(e.to_string()));
        }

        Ok(Response::new(TransactionResponse {
            message: format!("Transaction added in mempool: {:?}", tx_hash),
        }))
    }

    async fn send_raw_tx(
        &self,
        request: Request<RawTransaction>,
    ) -> Result<Response<TransactionResponse>, Status> {
        let raw_tx = &request.get_ref().raw_tx;
        let tx: Tx = serde_cbor::de::from_slice(&raw_tx).unwrap();
        let tx_hash = tx.hash();
        if let Err(e) = tx.verify_basic() {
            return Err(Status::invalid_argument(e.to_string()));
        }

        if let Err(e) = tx.verify_signature() {
            return Err(Status::invalid_argument(e.to_string()));
        }

        if let Err(e) = self.mempool.lock().unwrap().append_tx_and_broadcast(tx) {
            return Err(Status::internal(e.to_string()));
        }

        Ok(Response::new(TransactionResponse {
            message: format!("Transaction added in mempool: {:?}", tx_hash),
        }))
    }

    async fn get_account(
        &self,
        request: Request<AccountRequest>,
    ) -> Result<Response<AccountResponse>, Status> {
        let path: String = dirs::home_dir().unwrap().to_str().unwrap().to_owned() + "/mangostine";
        let value = &request.get_ref().address;
        println!("the address in service {:?}", value);
        let addr = address_from_string(value);
        let acc_store = AccountStore::open(path.as_str());
        let account = match acc_store.get_account(&addr.unwrap()) {
            Ok(account) => account,
            Err(e) => return Err(Status::invalid_argument(e.to_string())),
        };

        Ok(Response::new(AccountResponse {
            address: account.address.to_string(),
            balance: account.balance,
            sequence: account.sequence,
            flag: account.flag,
        }))
    }
}

impl Service {
    pub fn new(config: Config, mempool: Arc<Mutex<Mempool<AccountStoreProvider>>>) -> Self {
        Service { config, mempool }
    }

    pub async fn start_server(self) -> Result<(), Box<dyn std::error::Error>> {
        // defining address for our service
        let address = self.config.address.parse().unwrap();
        info!("Mangostine Server listening on {}", address);

        // adding our service to our server.
        let mut rt = Runtime::new().unwrap();
        let _r = rt.block_on(async move {
            if let Err(e) = Server::builder()
                .add_service(RpcServer::new(self))
                .serve(address)
                .await
            {
                error!("failed to read from socket; err = {:?}", e);
                return;
            }
        });

        Ok(())
    }
}
