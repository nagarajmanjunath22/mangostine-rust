use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Config {
    pub address: String,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            address: "0.0.0.0:16657".to_owned(),
        }
    }
}
