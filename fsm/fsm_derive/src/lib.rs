extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn;

#[proc_macro_derive(FsmTraitState)]
pub fn fsm_trait_state_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();
    impl_fsm_trait_state(&ast)
}

fn impl_fsm_trait_state(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl Copy for #name {}

        impl Clone for #name {
            fn clone(&self) -> #name {
                *self
            }
        }

        impl Eq for #name {}

        impl FsmTrait for #name {}
        impl FsmTraitState for #name {}
    };
    gen.into()
}

#[proc_macro_derive(FsmTraitEvent)]
pub fn fsm_trait_event_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();
    impl_fsm_trait_event(&ast)
}

fn impl_fsm_trait_event(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl Copy for #name {}

        impl Clone for #name {
            fn clone(&self) -> #name {
                *self
            }
        }

        impl Eq for #name {}

        impl FsmTrait for #name {}
        impl FsmTraitEvent for #name {}
    };
    gen.into()
}
