mod tests {
    use super::super::*;

    impl FsmTrait for i32 {}
    impl FsmTraitState for i32 {}
    impl FsmTraitEvent for i32 {}

    #[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
    enum MyTestState {
        Started,
        Initialized,
        Running,
        Terminated,
        NotUsed,
        Duplicated,
        ReDuplicated,
    }

    #[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
    enum MyTestEvent {
        OnAppInitialized,
        OnAppRunning,
        OnAppShutdown,
    }

    impl FsmTrait for MyTestState {}
    impl FsmTrait for MyTestEvent {}
    impl FsmTraitState for MyTestState {}
    impl FsmTraitEvent for MyTestEvent {}

    const FSM_NAME: &str = "myFSM";

    fn new_fsm_simple(name: &str) -> FiniteStateMachine<i32, i32> {
        let mut fsm = FiniteStateMachine::<i32, i32>::new(name.to_string());
        fsm.add_state("zero", &0);
        fsm.add_state("one", &1);
        fsm.add_state("two", &2);
        fsm.add_state("three", &3);

        fsm.add_event("satu", &1);
        fsm.add_event("dua", &2);
        fsm.add_event("tiga", &3);

        fsm.add_transition("zero", "satu", "one");
        fsm.add_transition("one", "dua", "two");
        fsm.add_transition("one", "tiga", "three");
        fsm.add_transition("three", "satu", "one");
        fsm.add_transition("three", "dua", "two");
        fsm.add_transition("three", "tiga", "three");

        fsm
    }

    fn new_fsm_custom(name: &str) -> FiniteStateMachine<MyTestState, MyTestEvent> {
        let mut fsm = FiniteStateMachine::<MyTestState, MyTestEvent>::new(name.to_string());
        fsm.add_state("s0", &MyTestState::Started);
        fsm.add_state("s1", &MyTestState::Initialized);
        fsm.add_state("s2", &MyTestState::Running);
        fsm.add_state("s3", &MyTestState::Terminated);

        fsm.add_event("t1", &MyTestEvent::OnAppInitialized);
        fsm.add_event("t2", &MyTestEvent::OnAppRunning);
        fsm.add_event("t3", &MyTestEvent::OnAppShutdown);

        fsm.add_transition("s0", "t1", "s1");
        fsm.add_transition("s0", "t3", "s3");
        fsm.add_transition("s1", "t2", "s2");
        fsm.add_transition("s1", "t3", "s3");

        fsm
    }

    #[test]
    fn fsm_simple_new_valid_name() {
        let fsm = new_fsm_simple(FSM_NAME);
        assert_eq!(fsm.name, FSM_NAME);
    }

    #[test]
    #[should_panic(expected = "Error: fsm name must not be empty")]
    fn fsm_simple_new_invalid_name() {
        new_fsm_simple("");
    }

    #[test]
    fn fsm_custom_new() {
        let fsm = new_fsm_custom(FSM_NAME);
        assert_eq!(fsm.name, FSM_NAME);
    }

    #[test]
    fn fsm_simple_add_state() {
        let fsm = new_fsm_simple(FSM_NAME);
        assert_eq!(fsm.state_count(), 4);

        let s1 = fsm.state("one");
        assert!(matches!(s1, FsmState::State(1)));
        assert!(!matches!(s1, FsmState::State(2)));
        assert!(matches!(fsm.state("special"), FsmState::InvalidState));
        assert_eq!(s1.name(&fsm), "one");
        assert_eq!(fsm.state_name(&s1), "one");
        assert_eq!(FsmCompState::name(&4i32, &fsm), "FsmInvalidState");
        assert_eq!(fsm.state("three").name(&fsm), "three");
    }

    #[test]
    fn fsm_simple_add_event() {
        let fsm = new_fsm_simple(FSM_NAME);
        assert_eq!(fsm.event_count(), 3);

        let t1 = fsm.event("satu");
        assert!(matches!(t1, FsmEvent::Event(1)));
        assert!(!matches!(t1, FsmEvent::Event(2)));
        assert_eq!(t1.name(&fsm), "satu");
        assert_eq!(fsm.event_name(&t1), "satu");
        assert_eq!(self::FsmCompEvent::name(&2, &fsm), "dua");
        assert_eq!(FsmCompEvent::name(&4i32, &fsm), "FsmInvalidEvent");
        assert_eq!(fsm.event("tiga").name(&fsm), "tiga");
    }

    #[test]
    #[should_panic]
    fn fsm_simple_add_duplicate_state_name() {
        let mut fsm = new_fsm_simple(FSM_NAME);
        fsm.add_state("four", &4);
        fsm.add_state("four", &44);
    }

    #[test]
    #[should_panic]
    fn fsm_simple_add_duplicate_state_value() {
        let mut fsm = new_fsm_simple(FSM_NAME);
        fsm.add_state("four", &4);
        fsm.add_state("fourfour", &4);
    }

    #[test]
    fn fsm_simple_initial_state() {
        let fsm = new_fsm_simple(FSM_NAME);
        assert!(matches!(fsm.initial_state(None), FsmState::State(0)));
        assert!(!matches!(fsm.initial_state(None), FsmState::State(1)));
        assert!(!matches!(fsm.initial_state(None), FsmState::InvalidState));
        assert!(matches!(
            fsm.initial_state(Some("DUMMY")),
            FsmState::InvalidState
        ));
        assert!(FsmState::State(0).is_initial_state(&fsm));
        assert!(!FsmState::State(1).is_initial_state(&fsm));
        assert!(!FsmState::InvalidState.is_initial_state(&fsm));
    }

    #[test]
    fn fsm_simple_initial_state_updated() {
        let mut fsm = new_fsm_simple(FSM_NAME);
        assert!(fsm.set_initial_state("two", None).unwrap());
        assert!(!matches!(fsm.initial_state(None), FsmState::State(0)));
        assert!(!matches!(fsm.initial_state(None), FsmState::State(1)));
        assert!(matches!(fsm.initial_state(None), FsmState::State(2)));
    }

    #[test]
    fn fsm_simple_final_state() {
        let fsm = new_fsm_simple(FSM_NAME);
        assert_eq!(fsm.is_final_state(&fsm.state("zero")).unwrap(), false);
        assert_eq!(fsm.is_final_state(&fsm.state("one")).unwrap(), false);
        assert_eq!(fsm.is_final_state(&fsm.state("two")).unwrap(), true);
        assert_eq!(fsm.is_final_state(&fsm.state("three")).unwrap(), false);

        assert!(!fsm.is_final_state(&FsmState::State(1)).unwrap());
        assert!(fsm.is_final_state(&FsmState::State(2)).unwrap());

        assert!(!FsmState::State(0).is_final_state(&fsm));
        assert!(!FsmState::State(1).is_final_state(&fsm));
        assert!(FsmState::State(2).is_final_state(&fsm));
        assert!(!FsmState::State(3).is_final_state(&fsm));
        assert!(!FsmState::State(4).is_final_state(&fsm));
    }

    #[test]
    #[should_panic]
    fn fsm_simple_final_state_invalid() {
        let fsm = new_fsm_simple(FSM_NAME);
        fsm.is_final_state(&FsmState::State(4)).unwrap();
    }

    #[test]
    fn fsm_simple_state_validity() {
        let fsm = new_fsm_simple(FSM_NAME);
        assert!(fsm.is_valid_state(&FsmState::State(0)));
        assert!(fsm.is_valid_state(&FsmState::State(1)));
        assert!(fsm.is_invalid_state(&FsmState::State(4)));
        assert!(fsm.is_invalid_state(&FsmState::State(5)));
        assert!(FsmState::State(0).is_valid_state(&fsm));
        assert!(FsmState::State(4).is_invalid_state(&fsm));
        assert!(0.is_valid_state(&fsm));
        assert!(4.is_invalid_state(&fsm));
    }

    #[test]
    fn fsm_simple_event_validity() {
        let fsm = new_fsm_simple(FSM_NAME);
        assert!(fsm.is_invalid_event(&FsmEvent::Event(0)));
        assert!(fsm.is_valid_event(&FsmEvent::Event(1)));
        assert!(fsm.is_invalid_event(&FsmEvent::Event(4)));
        assert!(fsm.is_invalid_event(&FsmEvent::Event(5)));

        assert!(FsmEvent::Event(0).is_invalid_event(&fsm));
        assert!(FsmEvent::Event(2).is_valid_event(&fsm));
        assert!(0.is_invalid_event(&fsm));
        assert!(2.is_valid_event(&fsm));
    }

    #[test]
    fn fsm_primitive_type() {
        let fsm = new_fsm_simple(FSM_NAME);
        let fsm2 = new_fsm_custom(FSM_NAME);

        println!("valid_event 0 is {}", 0.is_valid_event(&fsm));
        println!("valid_event 1 is {}", 1.is_valid_event(&fsm));
        println!("valid_event 2 is {}", 2.is_valid_event(&fsm));
        println!("valid_event 19 is {}", 19.is_valid_event(&fsm));

        println!("valid_state 0 is {}", 0.is_valid_state(&fsm));
        println!("valid_state 10 is {}", 10.is_valid_state(&fsm));
        
        println!("the name of state 1 is {}", 1.state_name(&fsm));
        println!("the name of event 1 is {}", 1.event_name(&fsm));

        println!("the name of state 10 is {}", 10.state_name(&fsm));
        println!("the name of event 10 is {}", 10.event_name(&fsm));

        println!("the name of MyTestState::Initialized is {}", &MyTestState::Initialized.name(&fsm2));
        println!("the name of MyTestState::Running is {}", &MyTestState::Running.name(&fsm2));
    }

    #[test]
    fn fsm_simple_next_state() {
        let fsm = new_fsm_simple(FSM_NAME);
        assert!(matches!(
            fsm.next_state_by_name("one", "dua"),
            FsmState::State(2)
        ));
        assert!(matches!(
            fsm.next_state_by_name("one", "tiga"),
            FsmState::State(3)
        ));
        assert!(matches!(
            fsm.next_state_by_name("two", "tiga"),
            FsmState::InvalidState
        ));
    }

    #[test]
    fn fsm_custom_add_state() {
        let fsm = new_fsm_custom(FSM_NAME);
        assert_eq!(fsm.state_count(), 4);

        let s1 = fsm.state("s1");
        assert!(matches!(s1, FsmState::State(MyTestState::Initialized)));
        assert!(!matches!(s1, FsmState::State(MyTestState::Running)));
        assert!(matches!(fsm.state("s999"), FsmState::InvalidState));
        assert_eq!(s1.name(&fsm), "s1");
        assert_eq!(fsm.state_name(&s1), "s1");
        assert_eq!(FsmState::State(MyTestState::Running).name(&fsm), "s2");
        assert_eq!(
            FsmState::State(MyTestState::NotUsed).name(&fsm),
            "FsmInvalidState"
        );
        assert_eq!(FsmState::InvalidState.name(&fsm), "FsmInvalidState");
        assert_eq!(fsm.state("s3").name(&fsm), "s3");
    }

    #[test]
    fn fsm_custom_add_event() {
        let fsm = new_fsm_custom(FSM_NAME);
        assert_eq!(fsm.event_count(), 3);

        let t1 = fsm.event("t1");
        assert!(matches!(t1, FsmEvent::Event(MyTestEvent::OnAppInitialized)));
        assert!(!matches!(t1, FsmEvent::Event(MyTestEvent::OnAppRunning)));
        assert!(matches!(fsm.event("t999"), FsmEvent::InvalidEvent));
        assert_eq!(t1.name(&fsm), "t1");
        assert_eq!(fsm.event_name(&t1), "t1");
        assert_eq!(FsmEvent::Event(MyTestEvent::OnAppRunning).name(&fsm), "t2");
        assert_eq!(FsmEvent::InvalidEvent.name(&fsm), "FsmInvalidEvent");
        assert_eq!(fsm.event("t3").name(&fsm), "t3");
    }

    #[test]
    #[should_panic]
    fn fsm_custom_add_duplicate_state_name() {
        let mut fsm = new_fsm_custom(FSM_NAME);
        fsm.add_state("duplicated", &MyTestState::Duplicated);
        fsm.add_state("duplicated", &MyTestState::ReDuplicated);
    }

    #[test]
    #[should_panic]
    fn fsm_custom_add_duplicate_state_value() {
        let mut fsm = new_fsm_custom(FSM_NAME);
        fsm.add_state("duplicated", &MyTestState::Duplicated);
        fsm.add_state("reduplicated", &MyTestState::Duplicated);
    }
}
