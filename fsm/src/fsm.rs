use log::info;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt::Debug;
use std::hash::Hash;

pub trait FsmTrait: Copy + Eq + Hash + Debug {}
pub trait FsmTraitState: FsmTrait {}
pub trait FsmTraitEvent: FsmTrait {}

pub trait FsmCompState<S: FsmTraitState> {
    fn state_name<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> String;
    fn name<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> String;
    fn is_initial_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool;
    fn is_final_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool;
    fn is_valid_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool;
    fn is_invalid_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool;
}

pub trait FsmCompEvent<T: FsmTraitEvent> {
    fn event_name<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> String;
    fn name<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> String;
    fn is_valid_event<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> bool;
    fn is_invalid_event<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> bool;
}

impl<S: FsmTraitState> FsmCompState<S> for S {
    fn state_name<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> String {
        fsm.state_name(&FsmState::State(self.clone()))
    }

    fn name<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> String {
        fsm.state_name(&FsmState::State(self.clone()))
    }

    fn is_initial_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        if let Ok(result) = fsm.is_initial_state(&self.name(&fsm), None) {
            result
        } else {
            false
        }
    }

    fn is_final_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        if let Ok(result) = fsm.is_final_state(&FsmState::State(self.clone())) {
            result
        } else {
            false
        }
    }

    fn is_valid_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        fsm.is_valid_state(&FsmState::State(self.clone()))
    }

    fn is_invalid_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        fsm.is_invalid_state(&FsmState::State(self.clone()))
    }
}

impl<S: FsmTraitState> FsmCompState<S> for FsmState<S> {
    fn state_name<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> String {
        match self {
            FsmState::State(_) => fsm.state_name(self),
            FsmState::InvalidState => "FsmInvalidState".to_string(),
        }
    }

    fn name<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> String {
        match self {
            FsmState::State(_) => fsm.state_name(self),
            FsmState::InvalidState => "FsmInvalidState".to_string(),
        }
    }

    fn is_initial_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        match self {
            FsmState::State(state) => {
                if let Ok(result) = fsm.is_initial_state(&state.name(&fsm), None) {
                    result
                } else {
                    false
                }
            }
            FsmState::InvalidState => false,
        }
    }

    fn is_final_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        if let Ok(result) = fsm.is_final_state(self) {
            result
        } else {
            false
        }
    }

    fn is_valid_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        fsm.is_valid_state(self)
    }

    fn is_invalid_state<T: FsmTraitEvent>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        fsm.is_invalid_state(self)
    }
}

impl<T: FsmTraitEvent> FsmCompEvent<T> for T {
    fn event_name<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> String {
        fsm.event_name(&FsmEvent::Event(self.clone()))
    }

    fn name<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> String {
        fsm.event_name(&FsmEvent::Event(self.clone()))
    }

    fn is_valid_event<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        fsm.is_valid_event(&FsmEvent::Event(self.clone()))
    }

    fn is_invalid_event<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        fsm.is_invalid_event(&FsmEvent::Event(self.clone()))
    }
}

impl<T: FsmTraitEvent> FsmCompEvent<T> for FsmEvent<T> {
    fn event_name<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> String {
        match self {
            FsmEvent::Event(_) => fsm.event_name(self),
            FsmEvent::InvalidEvent => "FsmInvalidEvent".to_string(),
        }
    }

    fn name<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> String {
        match self {
            FsmEvent::Event(_) => fsm.event_name(self),
            FsmEvent::InvalidEvent => "FsmInvalidEvent".to_string(),
        }
    }

    fn is_valid_event<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        fsm.is_valid_event(self)
    }

    fn is_invalid_event<S: FsmTraitState>(&self, fsm: &FiniteStateMachine<S, T>) -> bool {
        fsm.is_invalid_event(self)
    }
}

pub enum FsmState<T: FsmTraitState> {
    State(T),
    InvalidState,
}

pub enum FsmEvent<T: FsmTraitEvent> {
    Event(T),
    InvalidEvent,
}

pub struct StateTransition<StateImpl: FsmTraitState, EventImpl: FsmTraitEvent> {
    transition: HashMap<EventImpl, StateImpl>,
}

impl<StateImpl: FsmTraitState, EventImpl: FsmTraitEvent> StateTransition<StateImpl, EventImpl> {
    pub fn new() -> Self {
        StateTransition {
            transition: HashMap::new(),
        }
    }

    pub fn add_transition(&mut self, event: &EventImpl, next_state: &StateImpl) {
        if self.transition.contains_key(&event) {
            panic!("Error: cannot add non-deterministic transition");
        }
        self.transition.insert(event.clone(), next_state.clone());
    }

    pub fn remove_transition(&mut self, event: &EventImpl) {
        if !self.transition.contains_key(&event) {
            panic!("Error: cannot remove non-existent transition: {}");
        }
        self.transition.remove(&event);
    }

    pub fn is_final_state(&self) -> bool {
        self.transition.len() == 0
    }

    pub fn next_state(&self, event: &EventImpl) -> FsmState<StateImpl> {
        match self.transition.get(event) {
            Some(state) => FsmState::State(state.clone()),
            None => FsmState::InvalidState,
        }
    }

    pub fn transition_count(&self) -> usize {
        self.transition.len()
    }
}

pub struct FiniteStateMachine<StateImpl: FsmTraitState, EventImpl: FsmTraitEvent> {
    name: String,
    states: HashMap<String, StateImpl>,
    events: HashMap<String, EventImpl>,
    initial_states: HashMap<String, StateImpl>,
    state_table: HashMap<StateImpl, StateTransition<StateImpl, EventImpl>>,
    registered_states: HashSet<StateImpl>,
    registered_events: HashSet<EventImpl>,
}

impl<StateImpl: FsmTraitState, EventImpl: FsmTraitEvent> FiniteStateMachine<StateImpl, EventImpl> {
    pub fn new(name: String) -> Self {
        if name.trim() == "" {
            panic!("Error: fsm name must not be empty");
        }

        info!("fsm {} initialized", name);
        FiniteStateMachine {
            name,
            states: HashMap::new(),
            events: HashMap::new(),
            initial_states: HashMap::new(),
            state_table: HashMap::new(),
            registered_states: HashSet::new(),
            registered_events: HashSet::new(),
        }
    }

    pub fn add_state(&mut self, state_name: &str, state: &StateImpl) -> FsmState<StateImpl> {
        if state_name.len() == 0 {
            panic!("Error: invalid state name");
        }

        if self.states.contains_key(state_name) {
            panic!("Error: cannot add duplicate state: {}", state_name);
        }

        if self.registered_states.contains(state) {
            panic!("Error: state must be unique: {}", state_name);
        }

        self.states.insert(String::from(state_name), state.clone());
        self.registered_states.insert(state.clone());
        if self.initial_states.len() == 0 {
            self.initial_states.insert(self.name.clone(), state.clone());
        }
        FsmState::State(state.clone())
    }

    pub fn add_event(&mut self, event_name: &str, event: &EventImpl) -> FsmEvent<EventImpl> {
        if event_name.len() == 0 {
            panic!("Error: invalid event name");
        }

        if self.events.contains_key(event_name) {
            panic!("Error: cannot add duplicate event: {}", event_name);
        }

        if self.registered_events.contains(event) {
            panic!("Error: event must be unique: {}", event_name);
        }

        self.events.insert(String::from(event_name), event.clone());
        self.registered_events.insert(event.clone());
        FsmEvent::Event(event.clone())
    }

    pub fn add_transition(&mut self, state_name: &str, event_name: &str, next_state: &str) {
        match self.states.get(state_name) {
            Some(state) => match self.states.get(next_state) {
                Some(next_state_found) => match self.events.get(event_name) {
                    Some(event) => {
                        let state_table = self
                            .state_table
                            .entry(state.clone())
                            .or_insert(StateTransition::<StateImpl, EventImpl>::new());

                        state_table.add_transition(event, next_state_found);
                    }
                    None => panic!("Error: cannot find matching event: {}", event_name),
                },
                None => panic!("Error: cannot add transition to non-existent next state"),
            },
            None => panic!(
                "Error: cannot add transition for unregistered state: {}",
                state_name
            ),
        }
    }

    pub fn remove_transition(&mut self, state_name: &str, event_name: Option<&str>) {
        match self.state(state_name) {
            FsmState::State(state) => {
                if let Some(event_name_found) = event_name {
                    match self.event(event_name_found) {
                        FsmEvent::Event(event) => {
                            if let Some(state_table) = self.state_table.get_mut(&state) {
                                state_table.remove_transition(&event);
                            } else {
                                panic!(
                                    "Error: cannot remove transition for final state: {}",
                                    state_name
                                );
                            }
                        }
                        FsmEvent::InvalidEvent => {
                            panic!("Error: invalid find event: {}", event_name_found)
                        }
                    }
                } else {
                    if let None = self.state_table.get_mut(&state) {
                        panic!(
                            "Error: cannot remove transition for final state: {}",
                            state_name
                        );
                    }
                    self.state_table.remove(&state);
                }
            }
            FsmState::InvalidState => panic!("Error: cannot find state: {}", state_name),
        }
    }

    pub fn initial_state(&self, region_name: Option<&str>) -> FsmState<StateImpl> {
        let region: &str = match region_name {
            Some(name) => name,
            None => &self.name[..],
        };

        match self.initial_states.get(region) {
            Some(state) => FsmState::State(state.clone()),
            None => FsmState::InvalidState,
        }
    }

    pub fn set_initial_state(
        &mut self,
        state_name: &str,
        region_name: Option<&str>,
    ) -> Result<bool, String> {
        let region: &str = match region_name {
            Some(name) => name,
            None => &self.name[..],
        };

        match self.state(state_name) {
            FsmState::State(state) => {
                self.initial_states
                    .insert(String::from(region), state.clone());
                Ok(true)
            }
            FsmState::InvalidState => Err(format!(
                "Error: cannot set invalid state as initial state: {}",
                state_name
            )),
        }
    }

    pub fn state(&self, state_name: &str) -> FsmState<StateImpl> {
        match self.states.get(state_name) {
            Some(state) => FsmState::State(state.clone()),
            None => FsmState::InvalidState,
        }
    }

    pub fn state_name(&self, state: &FsmState<StateImpl>) -> String {
        match state {
            FsmState::State(s) => {
                for (key, value) in &self.states {
                    if value == s {
                        return key.clone();
                    }
                }
                "FsmInvalidState".to_string()
            }
            FsmState::InvalidState => "FsmInvalidState".to_string(),
        }
    }

    pub fn event(&self, event_name: &str) -> FsmEvent<EventImpl> {
        match self.events.get(event_name) {
            Some(event) => FsmEvent::Event(event.clone()),
            None => FsmEvent::InvalidEvent,
        }
    }

    pub fn event_name(&self, event: &FsmEvent<EventImpl>) -> String {
        match event {
            FsmEvent::Event(e) => {
                for (key, value) in &self.events {
                    if value == e {
                        return key.clone();
                    }
                }
                "FsmInvalidEvent".to_string()
            }
            FsmEvent::InvalidEvent => "FsmInvalidEvent".to_string(),
        }
    }

    pub fn next_state_by_name(&self, state_name: &str, event_name: &str) -> FsmState<StateImpl> {
        match self.state(state_name) {
            FsmState::State(state) => match self.event(event_name) {
                FsmEvent::Event(event) => match self.state_table.get(&state) {
                    Some(state_table) => state_table.next_state(&event),
                    None => FsmState::InvalidState,
                },
                FsmEvent::InvalidEvent => FsmState::InvalidState,
            },
            FsmState::InvalidState => FsmState::InvalidState,
        }
    }

    pub fn next_state(&self, state: &StateImpl, event: &EventImpl) -> FsmState<StateImpl> {
        if self.registered_states.contains(state) && self.registered_events.contains(event) {
            match self.state_table.get(&state) {
                Some(state_table) => state_table.next_state(&event),
                None => FsmState::InvalidState,
            }
        } else {
            FsmState::InvalidState
        }
    }

    pub fn is_initial_state(
        &self,
        state_name: &str,
        region_name: Option<&str>,
    ) -> Result<bool, String> {
        let region: &str = match region_name {
            Some(name) => name,
            None => &self.name[..],
        };

        match self.state(state_name) {
            FsmState::State(state) => match self.initial_state(Some(region)) {
                FsmState::State(initial_state) => Ok(state == initial_state),
                FsmState::InvalidState => Err(format!("Error: region does not exists: {}", region)),
            },
            FsmState::InvalidState => Err(format!("Error: state does not exists: {}", state_name)),
        }
    }

    pub fn is_final_state(&self, state: &FsmState<StateImpl>) -> Result<bool, String> {
        match state {
            FsmState::State(state) => {
                if self.registered_states.contains(state) {
                    match self.state_table.get(state) {
                        Some(state_table) => Ok(state_table.is_final_state()),
                        None => Ok(true),
                    }
                } else {
                    Err(format!("Error: invalid state"))
                }
            }
            FsmState::InvalidState => Err(format!("Error: invalid state")),
        }
    }

    pub fn is_valid_state(&self, state: &FsmState<StateImpl>) -> bool {
        match state {
            FsmState::State(state) => self.registered_states.contains(state),
            FsmState::InvalidState => false,
        }
    }

    pub fn is_invalid_state(&self, state: &FsmState<StateImpl>) -> bool {
        !self.is_valid_state(state)
    }

    pub fn is_valid_event(&self, event: &FsmEvent<EventImpl>) -> bool {
        match event {
            FsmEvent::Event(event) => self.registered_events.contains(event),
            FsmEvent::InvalidEvent => false,
        }
    }

    pub fn is_invalid_event(&self, event: &FsmEvent<EventImpl>) -> bool {
        !self.is_valid_event(event)
    }

    pub fn state_count(&self) -> usize {
        self.states.len()
    }

    pub fn event_count(&self) -> usize {
        self.events.len()
    }

    pub fn transition_count(&self, state_name: &str) -> usize {
        match self.state(state_name) {
            FsmState::State(state) => match self.state_table.get(&state) {
                Some(state_table) => state_table.transition_count(),
                None => 0,
            },
            FsmState::InvalidState => panic!("Error: state cannot be found: {}", state_name),
        }
    }
}

#[cfg(test)]
#[path = "./fsm_test.rs"]
mod fsm_test;
