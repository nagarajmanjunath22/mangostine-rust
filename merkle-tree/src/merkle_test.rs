#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;
    ///Create_data function create the data for testing the merkle tree
    fn create_data<'a, 'b>(n: i32) -> HashMap<&'a str, &'b str> {
        let mut list = HashMap::new();
        for elem in 1..n {
            list.insert("animal1", "cat");
            list.insert("animal2", "tiger");
            list.insert("animal3", "panther");
        }
        list
    }

    ///create  the merkle tree root hash without extension node
    #[test]
    fn merkle_test_root_no_extension() {
        let data_0 = create_data(1);
        let data_1 = create_data(1);
        let value_hash = trie_root_no_extension(data_0);
        let root_hash = trie_root_no_extension(data_1);
        assert_eq!(root_hash, value_hash);
    }

    //create  the merkle tree root with different number of transaction hash without extension node
    #[test]
    #[should_panic(
        expected = "Number of Transactions data must be same to create same merkle root hash"
    )]
    fn merkle_test_no_extension_fail() {
        let data_2 = create_data(1);
        let data_3 = create_data(5);
        let value_hash = trie_root_no_extension(data_2);
        let root_hash = trie_root_no_extension(data_3);
        assert_ne!(root_hash, value_hash);
    }

    //create  the merkle tree root hash with extension node
    #[test]
    fn merkle_test_root() {
        let data_4 = create_data(1);
        let data_5 = create_data(1);
        assert_eq!(trie_root(data_4), trie_root(data_5));
    }

    //create  the merkle tree root with different number of transaction hash with extension node
    #[test]
    #[should_panic(
        expected = "Number of Transactions data must be same to create same merkle root hash"
    )]
    fn merkle_test_root_fail() {
        let data_6 = create_data(1);
        let data_7 = create_data(5);
        let value_hash = trie_root(data_6);
        let root_hash = trie_root(data_7);
        assert_ne!(root_hash, value_hash);
    }
}