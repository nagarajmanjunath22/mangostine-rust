extern crate trie_root;
use crate::hasher;
use crate::triestream;
use hasher::blake3::Blake3Hasher as H;
use primitive_types::H256;
use triestream::MangostineTrieStream;

/// Generates a trie root hash for a vector of key-value tuples
pub fn trie_root<I, K, V>(input: I) -> H256
where
    I: IntoIterator<Item = (K, V)>,
    K: AsRef<[u8]> + Ord,
    V: AsRef<[u8]>,
{
    trie_root::trie_root::<H, MangostineTrieStream, _, _, _>(input)
}

/// Generates a trie root hash for a vector of values
pub fn trie_root_no_extension<I, K, V>(input: I) -> H256
where
    I: IntoIterator<Item = (K, V)>,
    K: AsRef<[u8]> + Ord,
    V: AsRef<[u8]>,
{
    trie_root::trie_root_no_extension::<H, MangostineTrieStream, _, _, _>(input)
}


