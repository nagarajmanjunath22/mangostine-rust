use blake3;

/// Do a Blake3 256-bit hash and place result in `dest`.
pub fn blake3_256_into(data: &[u8], dest: &mut [u8; 32]) {
	let mut hasher = blake3::Hasher::new();
	hasher.update(&data);
	let mut hash = hasher.finalize_xof();
	hash.fill(dest);
}
/// Do a Blake3 256-bit hash and return result.
pub fn blake3_256(data: &[u8]) -> [u8; 32] {
	let mut r = [0; 32];
	blake3_256_into(data, &mut r);
	r
}
