use parity_scale_codec::Encode;
use std::iter::once;
use trie_root::Hasher;
use trie_root::TrieStream;

const EMPTY_TRIE: u8 = 0;
const LEAF_NODE_OFFSET: u8 = 1;
const EXTENSION_NODE_OFFSET: u8 = 128;
const BRANCH_NODE_NO_VALUE: u8 = 254;
const BRANCH_NODE_WITH_VALUE: u8 = 255;
const LEAF_NODE_OVER: u8 = EXTENSION_NODE_OFFSET - LEAF_NODE_OFFSET;
const EXTENSION_NODE_OVER: u8 = BRANCH_NODE_NO_VALUE - EXTENSION_NODE_OFFSET;

#[derive(Default, Clone)]
pub struct MangostineTrieStream {
    buffer: Vec<u8>,
}

impl TrieStream for MangostineTrieStream {
    fn new() -> Self {
        MangostineTrieStream { buffer: Vec::new() }
    }

    fn append_empty_data(&mut self) {
        self.buffer.push(EMPTY_TRIE);
    }

    fn append_leaf(&mut self, key: &[u8], value: &[u8]) {
        self.buffer.extend(fuse_nibbles_node(key, true));
        value.encode_to(&mut self.buffer);
    }

    fn begin_branch(
        &mut self,
        maybe_key: Option<&[u8]>,
        maybe_value: Option<&[u8]>,
        has_children: impl Iterator<Item = bool>,
    ) {
        self.buffer
            .extend(&branch_node(maybe_value.is_some(), has_children));
        if let Some(partial) = maybe_key {
            // should not happen
            self.buffer.extend(fuse_nibbles_node(partial, false));
        }
        if let Some(value) = maybe_value {
            value.encode_to(&mut self.buffer);
        }
    }

    fn append_extension(&mut self, key: &[u8]) {
        self.buffer.extend(fuse_nibbles_node(key, false));
    }

    fn append_substream<H: Hasher>(&mut self, other: Self) {
        let data = other.out();
        match data.len() {
            0..=31 => data.encode_to(&mut self.buffer),
            _ => H::hash(&data).as_ref().encode_to(&mut self.buffer),
        }
    }

    fn out(self) -> Vec<u8> {
        self.buffer
    }
}

fn fuse_nibbles_node<'a>(nibbles: &'a [u8], leaf: bool) -> impl Iterator<Item = u8> + 'a {
    debug_assert!(
		nibbles.len() < LEAF_NODE_OVER.min(EXTENSION_NODE_OVER) as usize,
		"nibbles length too long. what kind of size of key are you trying to include in the trie!?!"
	);
    let first_byte = if leaf {
        LEAF_NODE_OFFSET
    } else {
        EXTENSION_NODE_OFFSET
    } + nibbles.len() as u8;

    once(first_byte)
        .chain(if nibbles.len() % 2 == 1 {
            Some(nibbles[0])
        } else {
            None
        })
        .chain(
            nibbles[nibbles.len() % 2..]
                .chunks(2)
                .map(|ch| ch[0] << 4 | ch[1]),
        )
}

fn branch_node(has_value: bool, has_children: impl Iterator<Item = bool>) -> [u8; 3] {
    let mut result = [0, 0, 0];
    branch_node_buffered(has_value, has_children, &mut result[..]);
    result
}

fn branch_node_buffered<I: Iterator<Item = bool>>(
    has_value: bool,
    has_children: I,
    output: &mut [u8],
) {
    let first = if has_value {
        BRANCH_NODE_WITH_VALUE
    } else {
        BRANCH_NODE_NO_VALUE
    };
    output[0] = first;
    Bitmap::encode(has_children, &mut output[1..]);
}

pub struct Bitmap(u16);

impl Bitmap {
    fn encode<I: Iterator<Item = bool>>(has_children: I, output: &mut [u8]) {
        let mut bitmap: u16 = 0;
        let mut cursor: u16 = 1;
        for v in has_children {
            if v {
                bitmap |= cursor
            }
            cursor <<= 1;
        }
        output[0] = (bitmap % 256) as u8;
        output[1] = (bitmap / 256) as u8;
    }
}
