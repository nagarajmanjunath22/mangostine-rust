/// Concrete `Hasher` impl for the blake3-256 hash
pub mod blake3 {
    //! A fixed hash type.
    use crate::hashing;
    use crunchy::unroll;
    use hash_db::Hasher;
    pub use primitive_types::{H160, H256, H512};
    use std::hash;

    /// Concrete implementation of Hasher using Blake3b 256-bit hashes
    #[derive(Debug)]
    pub struct Blake3Hasher;

    impl Hasher for Blake3Hasher {
        type Out = H256;
        type StdHasher = Hash256StdHasher;
        const LENGTH: usize = 32;

        fn hash(x: &[u8]) -> Self::Out {
            let v = hashing::blake3_256(x).into();
            v
        }
    }

    /// Hasher that just takes 8 bytes of the provided value.
    /// May only be used for keys which are 32 bytes.
    #[derive(Default)]
    pub struct Hash256StdHasher {
        prefix: u64,
    }

    impl hash::Hasher for Hash256StdHasher {
        #[inline]
        fn finish(&self) -> u64 {
            self.prefix
        }

        #[inline]
        #[allow(unused_assignments)]
        fn write(&mut self, bytes: &[u8]) {
            // we get a length written first as 8 bytes (possibly 4 on 32-bit platforms?). this
            // keeps it safe.
            debug_assert!(bytes.len() == 4 || bytes.len() == 8 || bytes.len() == 32);
            if bytes.len() < 32 {
                return;
            }

            let mut bytes_ptr = bytes.as_ptr();
            let mut prefix_ptr = &mut self.prefix as *mut u64 as *mut u8;

            unroll! {
                for _i in 0..8 {
                    unsafe {
                        *prefix_ptr ^= (*bytes_ptr ^ *bytes_ptr.offset(8)) ^ (*bytes_ptr.offset(16) ^ *bytes_ptr.offset(24));
                        bytes_ptr = bytes_ptr.offset(1);
                        prefix_ptr = prefix_ptr.offset(1);
                    }
                }
            }
        }
    }
}
