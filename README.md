# Mangostine Blockchain <img src="./mangosteen.png" width="30px" height="30px">

Mangostine is the private blockchain, Which is build on top raft consensus engine.

# Building Project

## Prerequisites

Mangostine requires latest stable Rust version to build. We recommend installing Rust

- Rust  1.47.0 [installed](https://www.rust-lang.org/)

### Source Code :

1. Clone the project repository

   `git clone https://gitlab.com/mxw-blockchain/mangostine-rust.git`

2. Change to the project directory.

   `cd gitlab.com/mxw-blockchain/mangostine-rust.git`

3. Build project.

   `cargo build`

### Initialize Mangostine :

- Mangostine *init* which create the genesis.json, config.toml and key.json file.

  `./target/debug/mangostine-cli init -o ~/mangostine`

### Run Mangostine :

- Mangostine  *start* which load the genesis.json and config.toml.

  `./target/debug/mangostine-cli start -o ~/mangostine`

### Sending Transaction :

- Mangostine *tx send* which get the account of sender, verify it and send the transaction.

  `./target/debug/mangostine-cli tx send-tx --sender 2100DC866C25C602FBF8A493BA3495E891F8E11E --receiver CCF1B22510C9300694FEF183F7575AE0A833BF90 --amount 10 --http "http://localhost:26657"`

_Note_: Please use the address which is available in key.json file and  *tx send* use grpc internally.

### Create Accounts :

- Mangostine *key generate* which create the account using ed25519 cryptography and save it to key.json.

  `./target/debug/mangostine-cli key generate -o ~/mangostine`

_Note_: Above command will ask you to input number of account need to be created.

### Test Mangostine :

- Mangostine *test* which run all module test .

  `cargo test`
