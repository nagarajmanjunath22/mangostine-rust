use crate::behaviour;
use crate::config::Config;
use crate::error::Error;
use crate::event::Event;
use crate::message_entry::MessageEntry;
use crate::transport;
use async_std::stream;
use async_std::sync::{channel, Receiver, Sender};
use behaviour::{Behaviour, BehaviourEventOut};
use futures::select;
use futures_util::stream::StreamExt;
pub use libp2p::gossipsub::{Topic, TopicHash};
use libp2p::identity;
use libp2p::Swarm;
use log::{error, info, trace, warn};
use std::collections::HashMap;
use std::time::Duration;
use types::NodeId;

pub struct Network {
    node_id: NodeId,
    config: Config,
    swarm: Swarm<Behaviour>,
    message_notifiers: HashMap<TopicHash, Sender<MessageEntry>>,
    message_receiver: Receiver<MessageEntry>,
    message_sender: Sender<MessageEntry>,
    event_receiver: Receiver<Event>,
    event_sender: Sender<Event>,
}

impl Network {
    pub fn new(config: Config, node_id: NodeId) -> Result<Self, Error> {
        let local_key = identity::Keypair::generate_ed25519();
        let local_public = local_key.public();
        let local_peer_id = local_public.clone().into_peer_id();
        info!("Local node identity is: {}", local_peer_id.to_base58());

        let transport = transport::build_transport(&local_key);
        let behaviour = Behaviour::new(&local_key, &config);

        let mut swarm = Swarm::new(transport, behaviour, local_peer_id);

        Swarm::listen_on(&mut swarm, config.listening_multiaddr.clone()).unwrap();

        for to_dial in &config.bootstrap_peers {
            match libp2p::Swarm::dial_addr(&mut swarm, to_dial.clone()) {
                Ok(_) => info!("Dialed {:?}", to_dial),
                Err(e) => error!("Dial {:?} failed: {:?}", to_dial, e),
            }
        }

        // Bootstrap with Kademlia
        if let Err(e) = swarm.bootstrap() {
            warn!("Failed to bootstrap with Kademlia: {}", e);
        }
        let (message_sender, message_receiver) = channel(30);
        let (event_sender, event_receiver) = channel(30);
        let message_notifiers = HashMap::new();

        Ok(Network {
            node_id,
            config,
            swarm,
            message_notifiers,
            message_sender,
            message_receiver,
            event_sender,
            event_receiver,
        })
    }

    pub fn close(&self) {}

    pub fn name(&self) -> String {
        self.config.network_name.clone()
    }
    
    pub fn sender(&self) -> Sender<MessageEntry> {
        self.message_sender.clone()
    }

    pub fn register_topic(&mut self, topic_name: String) -> Receiver<MessageEntry> {
        let topic = Topic::new(topic_name.clone());
        self.swarm.subscribe(topic.clone());
        let (notifier_sender, notifier_receiver) = channel(30);

        self.message_notifiers
            .insert(topic.no_hash(), notifier_sender);
        notifier_receiver
    }

    pub fn event_receiver(&self) -> Receiver<Event> {
        self.event_receiver.clone()
    }

    pub async fn run(self) {
        let mut swarm_stream = self.swarm.fuse();
        let mut network_stream = self.message_receiver.fuse();
        let mut interval = stream::interval(Duration::from_secs(10)).fuse();

        loop {
            select! {
                swarm_event = swarm_stream.next() => match swarm_event {
                    Some(event) => match event {
                        BehaviourEventOut::PeerConnected(peer_id) =>{
                            info!("Peer dialed {:?}", peer_id);
                            self.event_sender.send(Event::PeerConnected(peer_id)).await;
                        }
                        BehaviourEventOut::PeerDisconnected(peer_id) =>{
                            info!("Peer disconnected {:?}", peer_id);
                            self.event_sender.send(Event::PeerDisconnected(peer_id)).await;
                        }
                        BehaviourEventOut::GossipMessage {
                            source,
                            topics,
                            message,
                        } => {
                            trace!("Got a Gossip Envelope from {:?}", source);
                            for t in &topics {
                                let v = self.message_notifiers.get(t);
                                if let Some(rec) = self.message_notifiers.get(t) {
                                    let msg_info = message.clone();
                                    let entry = MessageEntry{
                                        topic_name: None,
                                        from: msg_info.to,
                                        to: msg_info.to,
                                        message: msg_info.message,
                                    };
                                    rec.send(entry).await;
                                }
                            }
                        }
                        BehaviourEventOut::BitswapReceivedBlock(peer_id, cid, block) => {

                        },
                        BehaviourEventOut::BitswapReceivedWant(peer_id, cid,) => {

                        },
                    }
                    None => { break; }
                },
                message = network_stream.next() => match message {
                    Some(mut entry) => {
                        if let Some(topic_name) = entry.topic_name.clone() {
                            let topic = Topic::new(topic_name);
                            entry.from = Some(self.node_id.clone());
                            if let Err(e) = swarm_stream.get_mut().publish(&topic, entry) {
                                warn!("Failed to send gossipsub message: {:?}", e);
                            }
                        }
                    }
                    None => { break; }
                },
                interval_event = interval.next() => if interval_event.is_some() {
                    trace!("Peers connected: {}", swarm_stream.get_ref().peers().len());
                }
            }
        }
    }
}

#[cfg(test)]
#[path = "./network_test.rs"]
mod network_test;
