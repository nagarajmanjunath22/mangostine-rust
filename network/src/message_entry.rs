use serde::{Deserialize, Serialize};
use serde_cbor::from_slice;
use serde_cbor::to_vec;
use std::vec::Vec;
use types::NodeId;

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct MessageEntry {
    pub topic_name: Option<String>,
    pub from: Option<NodeId>,
    pub to: Option<NodeId>,
    pub message: Vec<u8>,
}

impl MessageEntry {
    pub fn new(topic_name: String, to: NodeId, message: Vec<u8>) -> Self {
        MessageEntry {
            topic_name: Some(topic_name),
            from: None,
            to: Some(to),
            message,
        }
    }
}

impl Into<Vec<u8>> for MessageEntry {
    fn into(self) -> Vec<u8> {
        to_vec(&self).unwrap()
    }
}

impl Into<MessageEntry> for Vec<u8> {
    fn into(self) -> MessageEntry {
        from_slice(&self).unwrap()
    }
}
