pub mod transaction;

use serde::{Deserialize, Serialize};
use serde_cbor::from_slice;
use serde_cbor::to_vec;
pub use transaction::TransactionMsg;
use std::convert::TryFrom;
use error::error::MgError;



/// A network message.
#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub enum Message {
    Transaction(TransactionMsg),
}

impl From<Message> for Vec<u8> {
    fn from(tx: Message) -> Vec<u8> {
        to_vec(&tx).unwrap()
    }
}

impl TryFrom<Vec<u8>> for Message {
    type Error = MgError;

    fn try_from(data: Vec<u8>) -> Result<Message, MgError> {
        from_slice(&data).map_err(|_| MgError::ParsingError)
    }
}