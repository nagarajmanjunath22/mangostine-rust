use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Config{
    pub max_size: usize,
}

impl Default for Config {
    fn default() -> Self {
        Self{
            max_size:  10000,
        }
    }
}
