mod mempool_test {

    use super::super::*;
    use std::str::FromStr;
    use types::address::Address;
    use types::transaction::Transaction;
    use types::account::Account;

    #[test]
    fn mempool_test_invalid_seq() {
        let addr1 = Address::from_str("1a77bb6d5382098474331580b8d137705b0271b1").unwrap();
        let addr2 = Address::from_str("1d55bb6d5382098474331580b8d137705b0551b1").unwrap();
        let acc = Account::new(&addr1, 100, 0, false);

        let config = crate::config::Config::default();
        let mut provider = TestProvider::new();
        provider.add_account(acc);
        let mut mempool = crate::mempool::Mempool::new(config, provider).unwrap();

        let tx1 = Transaction::new(addr1.clone(), addr2.clone(), 50, 0);
        let tx2 = Transaction::new(addr1.clone(), addr2.clone(), 60, 1);
        let tx3 = Transaction::new(addr2.clone(), addr1.clone(), 60, 0);
        // account not exist
        assert!(mempool.append_tx(tx3.clone()).is_err());

        // Sequence is wrong
        assert!(mempool.append_tx(tx2.clone()).is_err());

        // account exist, ok
        assert!(mempool.append_tx(tx1.clone()).is_ok());

        // Insufficient balance
        assert!(mempool.append_tx(tx2.clone()).is_err());

    }
}
