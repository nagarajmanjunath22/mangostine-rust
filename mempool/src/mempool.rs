use crate::config::Config;
use crate::provider::Provider;
use async_std::sync::{channel, Receiver, Sender};
use error::error::MgError;
use types::transaction::Transaction;

#[derive(Debug, Clone)]
pub struct Mempool<T> {
    config: Config,
    provider: T,
    txs: Vec<Transaction>,
    tx_sender: Sender<Transaction>,
    tx_receiver: Receiver<Transaction>,
}

impl<T> Mempool<T>
where
    T: Provider,
{
    pub fn new(config: Config, provider: T) -> Result<Self, MgError> {
        let (tx_sender, tx_receiver) = channel(100);
        Ok(Mempool {
            provider,
            config,
            txs: Vec::new(),
            tx_sender,
            tx_receiver,
        })
    }

    pub fn tx_receiver(&self) -> Receiver<Transaction> {
        self.tx_receiver.clone()
    }

    pub fn append_tx_and_broadcast(&mut self, tx: Transaction) -> Result<(), MgError> {
        self.append_tx(tx.clone())?;
        self.tx_sender
            .try_send(tx)
            .map_err(|_e| MgError::BroadcastMsgFailed)?;

        Ok(())
    }

    pub fn append_tx(&mut self, tx: Transaction) -> Result<(), MgError> {
        if self.txs.len() > self.config.max_size {
            return Err(MgError::Other("Mempool is full".to_owned()));
        }

        tx.verify_basic()?;
        tx.verify_signature()?;

        let acc = self.provider.account(&tx.payload.sender)?;

        if tx.payload.sequence != acc.sequence {
            return Err(MgError::InvalidTx(format!(
                "The sequence is invalid. Need {}, got {}.",
                acc.sequence + 1,
                tx.payload.sequence
            )));
        }

        let total_amount = tx.total_amount();
        if total_amount > acc.balance {
            return Err(MgError::InvalidTx(format!(
                "TThe balance is not enough. Need {}, has {}.",
                total_amount, acc.balance
            )));
        }

        acc.sequence += 1;
        acc.balance -= total_amount;

        self.txs.push(tx);
        Ok(())
    }

    pub fn len(&self) -> usize {
        self.txs.len()
    }
}

#[cfg(test)]
#[path = "./mempool_test.rs"]
mod mempool_test;
