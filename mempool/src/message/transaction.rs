use serde::{Deserialize, Serialize};
use types::transaction::Transaction;

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct TransactionMsg {
    pub tx: Transaction,
}
