use error::error::MgError;
use persistence::account_store::AccountStore;
use std::collections::HashMap;
use types::account::Account;
use types::address::Address;
use std::collections::hash_map::Entry::{Occupied, Vacant};

pub trait Provider {
    fn account(&mut self, addr: &Address) -> Result<&mut Account, MgError>;
}

pub struct AccountStoreProvider {
    store: AccountStore,
    cache: HashMap<Address, Account>,
}

impl AccountStoreProvider {
    pub fn new(store: AccountStore) -> Self {
        AccountStoreProvider {
            store,
            cache: HashMap::new()
        }
    }
}

impl Provider for AccountStoreProvider{
    fn account(&mut self, addr: &Address) -> Result<&mut Account, MgError> {

        // TODO:
        // Can we not clone address here ?
        return match self.cache.entry(addr.clone()) {
            Vacant(entry) => {
                let acc = self.store.get_account(&addr)?;
                 Ok(entry.insert(acc))
            },
            Occupied(entry) => {
                 Ok(entry.into_mut())
            }
        };
    }
}



pub struct TestProvider {
    cache: HashMap<Address, Account>,
}

impl TestProvider {
    pub fn new() -> Self {
        TestProvider {
            cache: HashMap::new()
        }
    }

    pub fn add_account(&mut self, acc: Account) {
        self.cache.insert(acc.address.clone(), acc);
    }
}

impl Provider for TestProvider{
    fn account(&mut self, addr: &Address) -> Result<&mut Account, MgError> {

        return match self.cache.entry(addr.clone()) {
            Vacant(_entry) => {
                Err(MgError::NotExists)
            },
            Occupied(entry) => {
                 Ok(entry.into_mut())
            }
        };
    }
}


#[cfg(test)]
#[path = "./provider_test.rs"]
mod mempool_test;
