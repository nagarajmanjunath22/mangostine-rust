use crate::message::Message;
use crate::{Mempool, AccountStoreProvider};
use async_std::stream;
use async_std::sync::{Receiver, Sender};
use error::error::MgError;
use futures::select;
use futures_util::stream::StreamExt;
use log::{error, info};
use network::{MessageEntry, Network};
use std::convert::TryFrom;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use types::transaction::Transaction;

pub struct Service {
    mempool: Arc<Mutex<Mempool<AccountStoreProvider>>>,
    topic_name: String,
    message_sender: Sender<MessageEntry>,
    message_receiver: Receiver<MessageEntry>,
    tx_receiver: Receiver<Transaction>,
}

impl Service {
    pub fn new(mempool: Arc<Mutex<Mempool<AccountStoreProvider>>>, network: &mut Network) -> Result<Self, MgError> {
        let topic_name = "mempool".to_owned();

        let tx_receiver = mempool.lock().unwrap().tx_receiver();
        let message_sender = network.sender();
        let message_receiver = network.register_topic(topic_name.clone());

        Ok(Service {
            mempool,
            topic_name,
            message_sender,
            message_receiver,
            tx_receiver,
        })
    }

    pub async fn start(self) {
        let mut mempool_stream = self.tx_receiver.fuse();
        let mut network_stream = self.message_receiver.fuse();
        let mut interval = stream::interval(Duration::from_secs(10)).fuse();

        loop {
            select! {
                message = network_stream.next() => match message {
                    Some(entry) => {
                        info!("Received message from network: {}", hex::encode(&entry.message));

                        match Message::try_from(entry.message) {
                            Ok(msg) => {
                                match msg {
                                    Message::Transaction(tx_msg) => {
                                        if let Err(e) = self.mempool.lock().unwrap().append_tx(tx_msg.tx) {
                                            info!("Transaction rejected by mempool: {}", e);
                                        }
                                    }
                                    _ => {}
                                }
                            }
                            Err(e) => {
                                error!("Invalid transaction: {}", e);
                            }
                        }
                    }
                    None => {
                        break;
                    }
                },

                tx = mempool_stream.next() => match tx {
                    Some(tx) => {
                        info!("Received tx from mempool");
                        let msg = Message::Transaction(crate::message::TransactionMsg {
                            tx: tx.clone(),
                        });
                        let msg_entry =
                            MessageEntry::new(self.topic_name.clone(), 0, msg.into());
                        self.message_sender.send(msg_entry).await;
                    }
                    None => {break;}
                },
                interval_event = interval.next() => if interval_event.is_some() {
                    info!("Mempool size {}", self.mempool.lock().unwrap().len());
                }

            }
        }
    }
}
