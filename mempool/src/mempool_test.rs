
#[cfg(test)]
mod mempool_test {

    use super::super::*;

    use std::str::FromStr;
    use std::collections::HashMap;
    use crypto::keypair::Keypair;
    use types::address::Address;
    use types::transaction::{Transaction, Recipient};

    use types::account::Account;
    use persistence::account_store::AccountStore;
    use crate::provider::TestProvider;
    use crate::provider::AccountStoreProvider;
    use persistence::store::{Database, KVStore};


    #[test]
    fn mempool_test_append_valid_tx_with_single_receiver() {

        let sender_addr = Address::from_str("1a77bb6d5382098474331580b8d137705b0271b1").unwrap();
        let sender_acccount= Account::new(&sender_addr, 10000, 0, false);

        let receiver_addr1 = Address::from_str("1d55bb6d5382098474331580b8d137705b0551b1").unwrap();
        let receiver_amt1 = u64::from_str("1000").unwrap();

        let tx_seq1 = 0;            // with Trx_seq = 0

        let mut tx = Transaction::new(sender_addr, receiver_addr1, receiver_amt1, tx_seq1);
        let sender_keypair = Keypair::new();
        let pub_key = sender_keypair.public().to_owned();

        let to_sign = tx.sign_bytes();
        let signature = sender_keypair.try_sign(&to_sign).unwrap();

        //update with sender-key, sender-signature
        tx.public_key = Some(pub_key);
        tx.signature = Some(signature);

        let _tx_hash = tx.hash();
        assert_eq!(tx.verify_basic().is_err(), false);
        assert_eq!(tx.verify_signature().is_err(), false);

        let mut provider = TestProvider::new();
        provider.add_account(sender_acccount);


        let mut mempool = Mempool::new(Config::default(), provider).ok().unwrap();
        assert_eq!(mempool.append_tx(tx).is_ok(), true);
        assert_eq!(mempool.len(), 1);


    }



    #[test]
    fn mempool_test_append_valid_tx_with_multiple_receiver() {

        let sender_addr = Address::from_str("1a88bb6d5382098474331580b8d137705b0271b3").unwrap();
        let sender_acccount= Account::new(&sender_addr, 230000, 0, false);

        let mut receiver: Vec<Recipient> = Vec::new();
        let receiver_addr1 = Address::from_str("1d33bb6d5382098474331580b8d137705b0333b1").unwrap();
        let receiver_addr2 = Address::from_str("2d33bb6d5382098474331580b8d137705b0333b2").unwrap();
        let receiver_addr3 = Address::from_str("3d33bb6d5382098474331580b8d137705b0333b3").unwrap();
        let receiver_addr4 = Address::from_str("4d33bb6d5382098474331580b8d137705b0333b4").unwrap();
        let receiver_addr5 = Address::from_str("5d33bb6d5382098474331580b8d137705b0333b5").unwrap();

        let receiver_amt1 = u64::from_str("11111").unwrap();
        let receiver_amt2 = u64::from_str("22").unwrap();
        let receiver_amt3 = u64::from_str("31").unwrap();
        let receiver_amt4 = u64::from_str("444").unwrap();
        let receiver_amt5 = u64::from_str("5511").unwrap();

        let mut send_to = HashMap::new();
        send_to.insert(receiver_addr1,receiver_amt1);
        send_to.insert(receiver_addr2,receiver_amt2);
        send_to.insert(receiver_addr3,receiver_amt3);
        send_to.insert(receiver_addr4,receiver_amt4);
        send_to.insert(receiver_addr5,receiver_amt5);

        for (key, val) in send_to.iter() {
            let send = Recipient {
                address: key.to_owned(),
                amount: val.to_owned(),
            };
            receiver.push(send);
        }

        let tx_seq1 = 0;            // with Trx_seq = 0

        let mut tx = Transaction::new_multi_recipients(sender_addr, receiver, tx_seq1, None, None);
        let keypair = Keypair::new();
        let pub_key = keypair.public().to_owned();

        let to_sign = tx.sign_bytes();
        let signature = keypair.try_sign(&to_sign).unwrap();

        //update with sender-key, sender-signature
        tx.public_key = Some(pub_key);
        tx.signature = Some(signature);


        let _tx_hash = tx.hash();
        assert_eq!(tx.verify_basic().is_err(), false);
        assert_eq!(tx.verify_signature().is_err(), false);

        let mut provider = TestProvider::new();
        provider.add_account(sender_acccount);


        let mut mempool = Mempool::new(Config::default(), provider).ok().unwrap();
        assert_eq!(mempool.append_tx(tx).is_ok(), true);
        assert_eq!(mempool.len() == 1, true);


    }


    #[test]
    fn mempool_test_append_tx_with_invalid_signature() {

        let sender_addr = Address::from_str("1a88bb6d5382098474331580b8d137705b0271b3").unwrap();
        let sender_acccount= Account::new(&sender_addr, 230000, 0, false);

        let mut receiver: Vec<Recipient> = Vec::new();
        let receiver_addr1 = Address::from_str("1d33bb6d5382098474331580b8d137705b0333b1").unwrap();
        let receiver_addr2 = Address::from_str("2d33bb6d5382098474331580b8d137705b0333b2").unwrap();
        let receiver_addr3 = Address::from_str("3d33bb6d5382098474331580b8d137705b0333b3").unwrap();


        let receiver_amt1 = u64::from_str("11111").unwrap();
        let receiver_amt2 = u64::from_str("22").unwrap();
        let receiver_amt3 = u64::from_str("31").unwrap();

        let mut send_to = HashMap::new();
        send_to.insert(receiver_addr1,receiver_amt1);
        send_to.insert(receiver_addr2,receiver_amt2);
        send_to.insert(receiver_addr3,receiver_amt3);

        for (key, val) in send_to.iter() {
            let send = Recipient {
                address: key.to_owned(),
                amount: val.to_owned(),
            };
            receiver.push(send);
        }

        let tx_seq1 = 0;            // with Trx_seq = 0


        let mut tx = Transaction::new_multi_recipients(sender_addr, receiver, tx_seq1,  None, None);
        let keypair = Keypair::new();
        let pub_key = keypair.public().to_owned();

        let to_sign = tx.sign_bytes();
        let _signature = keypair.try_sign(&to_sign).unwrap();

        //update with sender-key, sender-signature
        tx.public_key = Some(pub_key);
        tx.signature = None;

        let _tx_hash = tx.hash();
        assert_eq!(tx.verify_basic().is_err(), true);
        assert_eq!(tx.verify_signature().is_err(), true);


        let mut provider = TestProvider::new();
        provider.add_account(sender_acccount);


        let mut mempool = Mempool::new(Config::default(), provider).ok().unwrap();
        assert_eq!(mempool.append_tx(tx).is_ok(), true);
        assert_eq!(mempool.len() > 1, false);


    }


    #[test]
    fn mempool_test_append_tx_by_validate_sender_account_balance() {

        let account_store: AccountStore = AccountStore::open("test1-rocksdb");

        //1. Create sender-account with enough balance amount
        let sender_addr = Address::from_str("7b33bb6d5382098474331580b8d137705b0273b3").unwrap();
        let sender_acc = get_sender_account(&account_store, &sender_addr);
        println!("Sender-account data : {:?}", sender_acc);

        if sender_acc.is_ok(){

            let sender_account = sender_acc.unwrap();

            let receiver_addr1 = Address::from_str("1e33bb6d5382098474331580b8d137705b0333b1").unwrap();
            let receiver_amt1 = u64::from_str("1999").unwrap();
            println!("Receiver total amount: {:?}", receiver_amt1);
            let tx_seq1 = 1;            // with Trx_seq = 1

            let resp = compile_tx(&account_store, sender_addr, receiver_addr1, receiver_amt1, tx_seq1);
            assert_eq!(resp.is_ok(), true);

            let tx = resp.unwrap();

            let receiver_total_amount = u64::from_str(&tx.total_amount().to_string()).unwrap();
            let sender_balance = sender_account.balance;
            assert_eq!(sender_balance > receiver_total_amount, true);

            let provider = AccountStoreProvider::new(account_store);


            let mut mempool = Mempool::new(Config::default(), provider).ok().unwrap();
            let rs = mempool.append_tx(tx);
            assert_eq!(rs.is_ok(), true);
            assert_eq!(mempool.len() == 1, true);


        }

        KVStore::destroy("test1-rocksdb/data/mangostine_account_store");
        KVStore::destroy("test1-rocksdb/data");
        KVStore::destroy("test1-rocksdb");

    }


    #[test]
    fn mempool_test_append_multiple_valid_txs() {

        let account_store: AccountStore = AccountStore::open("test2-rocksdb");

        let sender_addr = Address::from_str("8a77bb6d5382098474331580b8d137705b0271b1").unwrap();
        let sender_acc = get_sender_account(&account_store, &sender_addr);
        println!("Sender-account data : {:?}", sender_acc);

        if sender_acc.is_ok(){

            let sender_account = sender_acc.unwrap();
            let receiver_addr1 = Address::from_str("1d11bb6d5382098474331580b8d137705b0333b1").unwrap();
            let receiver_amt1 = u64::from_str("122").unwrap();
            let tx_seq1 = 1;            // with Trx_seq = 1
            let resp_tx1 = compile_tx(&account_store, sender_addr.clone(), receiver_addr1, receiver_amt1, tx_seq1);
            assert_eq!(resp_tx1.is_ok(), true);
            let tx1 = resp_tx1.unwrap();


            let receiver_addr2 = Address::from_str("1d22bb6d5382098474331580b8d137705b0333b1").unwrap();
            let receiver_amt2 = u64::from_str("99").unwrap();
            let tx_seq2 = 2;            // with Trx_seq = 2
            let resp_tx2 = compile_tx(&account_store, sender_addr.clone(), receiver_addr2, receiver_amt2, tx_seq2);
            assert_eq!(resp_tx2.is_ok(), true);
            let tx2 = resp_tx2.unwrap();

            let receiver_addr3 = Address::from_str("1d33bb6d5382098474331580b8d137705b0333b1").unwrap();
            let receiver_amt3 = u64::from_str("51").unwrap();
            let tx_seq3 = 3;            // with Trx_seq = 3
            let resp_tx3 = compile_tx(&account_store, sender_addr.clone(), receiver_addr3, receiver_amt3, tx_seq3);
            assert_eq!(resp_tx3.is_ok(), true);
            let tx3 = resp_tx3.unwrap();


            let total_amount = tx1.total_amount() + tx2.total_amount() + tx3.total_amount();
            let receiver_total_amount = u64::from_str(&total_amount.to_string()).unwrap();
            let sender_balance = sender_account.balance;
            assert_eq!(sender_balance > receiver_total_amount, true);


            let provider = AccountStoreProvider::new(account_store);

            let mut mempool = Mempool::new(Config::default(), provider).ok().unwrap();
            let rs_tx1 = mempool.append_tx(tx1);
            assert_eq!(rs_tx1.is_ok(), true);
            assert_eq!(mempool.len() == 1, true);


            let rs_tx2 = mempool.append_tx(tx2);
            assert_eq!(rs_tx2.is_ok(), true);
            assert_eq!(mempool.len() == 2, true);

            let rs_tx3 = mempool.append_tx(tx3);
            assert_eq!(rs_tx3.is_ok(), true);
            assert_eq!(mempool.len() == 3, true);

            assert_eq!(mempool.len() > 1, true);

        }

        KVStore::destroy("test2-rocksdb/data/mangostine_account_store");
        KVStore::destroy("test2-rocksdb/data");
        KVStore::destroy("test2-rocksdb");

    }


    //sub-method
    fn compile_tx (account_store: &AccountStore, sender: Address, receiver: Address, amt: u64, tx_seq: u64) -> Result<Transaction, MgError>{

        let mut tx = Transaction::new(sender.clone(), receiver, amt, tx_seq);
        let sender_keypair = Keypair::new();
        let pub_key = sender_keypair.public().to_owned();

        let to_sign = tx.sign_bytes();
        let signature = sender_keypair.try_sign(&to_sign).unwrap();

        //update with sender-key, sender-signature
        tx.public_key = Some(pub_key);
        tx.signature = Some(signature);

        if let Err(e) = tx.verify_basic() {
            return Err(MgError::InvalidTx(e.to_string()));
        }
        if let Err(e) = tx.verify_signature() {
            return Err(MgError::InvalidTx(e.to_string()));
        }

        let receiver_total_amount = tx.total_amount();

        let sender_account = get_sender_account(account_store, &sender.clone());
        if let Err(e) = sender_account{
            return Err(MgError::InvalidTx(e.to_string()));
        }

        let sender_balance = sender_account.unwrap().balance.to_owned();
        let total_amount = u64::from_str(&receiver_total_amount.to_string()).unwrap();
        if sender_balance <= total_amount{           // checking validate : sender bal > receivers amount
            let err = format!("{}{}", "sender balance required this amount to do P2P: ", receiver_total_amount);
            return Err(MgError::InvalidTx(err));
        }

        return Ok(tx);
    }

    //sub-method
    fn get_sender_account(account_store: &AccountStore, address: &Address) -> Result<Account, MgError> {

        let querry_account = account_store.get_account(&address);
        if querry_account.is_err(){

            let sender_account = account_store.new_account(&address);
            let mut sender_acc = sender_account.unwrap();
            sender_acc.balance = sender_acc.balance + 200000;
            sender_acc.sequence = sender_acc.sequence + 1;

            let account = account_store.save_account(sender_acc).unwrap();
            return Ok(account);
        }

        return Ok(querry_account.unwrap());
    }


}
