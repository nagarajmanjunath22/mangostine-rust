#[cfg(test)]
mod tests {
    use crate::address::Address;
    use crypto::keypair::Keypair;
    use rand::rngs::OsRng;
    use std::str::FromStr;
    use serde_cbor::from_slice;
    use serde_cbor::to_vec;
    use hex;


    ///Address length must be 20 bytes
    #[test]
    fn address_length() {
        let mut csprng = OsRng{};
        let kp = Keypair::generate(&mut csprng);
        let addr = Address::from(kp.public().to_owned());
        assert_eq!(addr.as_bytes().len(), 20)
    }

    ///Address  comparison
    #[test]
    fn address_compare() {
        let pub_hex = "cd5fb87a244dacaf83b31f663185b894cbeb2fd2952f9770bea62744e3c4d399";
        let addr_hex = "1a77bb6d5382098474331580b8d137705b0271b1";
        let pubkey_bytes = &hex::decode(pub_hex).unwrap_or_default();

        let pub_key = Keypair::from_bytes(pubkey_bytes).unwrap();
        let addr = Address::from(pub_key.public().to_owned());
        assert_eq!(addr, Address::from_str(addr_hex).unwrap());
    }

    ///Address length must be 20 bytes
    #[test]
    fn address_length_fail() {
        let (kp, _mnemonic) = Keypair::generate_with_seed();
        let addr = Address::from(*kp.public());
        println!("The value in {:?}", addr);
        assert_ne!(addr.as_bytes().len(), 21)
    }

    ///Address  comparison it will fail the address is different
    #[test]
    fn address_compare_fail() {
        let pub_hex = "cd5fb87a244dacaf83b31f663185b894cbeb2fd2952f9770bea62744e3c4d399";
        let addr_hex = "1a77bb6d5382098474331580b8d137705b0271bb";
        let pubkey_bytes = &hex::decode(pub_hex).unwrap_or_default();

        let pub_key = Keypair::from_bytes(pubkey_bytes).unwrap();
        let addr = Address::from(pub_key.public().to_owned());
        assert_ne!(addr, Address::from_str(addr_hex).unwrap());
    }

    /// Address function display info
    #[test]
    fn address_by_display_debug() {
        let addr = Address::from_str("2b77bb6d5382098474331580b8d137705b0271b2").unwrap();
        println!("Address is : {:?}", format!("{}", addr));

        assert_eq!(
            "2B77BB6D5382098474331580B8D137705B0271B2",
            format!("{}", addr)
        );
    }

    /// Address function as ref value
    #[test]
    fn address_by_as_ref() {
        let addr = Address::from_str("2b77bb6d5382098474331580b8d137705b0271b2").unwrap();

        assert_eq!(
            [
                43, 119, 187, 109, 83, 130, 9, 132, 116, 51, 21, 128, 184, 209, 55, 112, 91, 2,
                113, 178
            ],
            addr.as_ref()
        );
    }

    #[test]
    fn address_cbor_encode() {
        //let addr = Address::from_str("000102030482098474331580b8d137705b0271b2").unwrap();
        let addr :Vec<u8>= vec![0x00,0x01,0x02,0x03,0x04,0x82,0x09,0x84,0x74,0x33,0x15,0x80,0xb8,0xd1,0x37,0x70,0x5b,0x02,0x71,0xb2];
        let res = to_vec(&addr[0..20]).unwrap();
        println!("{}", hex::encode(res));
    }
}
