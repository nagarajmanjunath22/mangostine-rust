#[cfg(test)]
mod tests {
    use super::super::*;
    use crypto::keypair::Keypair;
    use std::convert::From;
    use std::str::FromStr;
    use Address;

    #[test]
    fn transaction_test_validate_signature() {
        let keypair = Keypair::new();
        let pub_key = keypair.public();

        let address = Address::new(Default::default()).clone();

        let transaction = Transaction {
            payload: Payload {
                sequence: 0,
                sender: address.clone(),
                recipients: vec![Recipient {
                    address: address.clone(),
                    amount: u64::from_str_radix("A", 16).unwrap(),
                }],
            },
            public_key: Some(pub_key.to_owned()),
            signature: None,
        };

        let to_sign = transaction.sign_bytes();
        let signature = keypair.try_sign(&to_sign).unwrap();

        let transaction_to_validate = Transaction {
            payload: Payload {
                sequence: 0,
                sender: address.clone(),
                recipients: vec![Recipient {
                    address: address.clone(),
                    amount: u64::from_str_radix("A", 16).unwrap(),
                }],
            },
            public_key: Some(pub_key.to_owned()),
            signature: Some(signature),
        };

        // expected correct signature.
        assert_eq!(transaction_to_validate.verify_signature().is_err(), false);
        assert_eq!(transaction_to_validate.verify_signature().is_ok(), true);

        let keypair_1 = Keypair::new();
        let pub_key_1 = keypair_1.public();

        let transaction_1 = Transaction {
            payload: Payload {
                sequence: 0,
                sender: address.clone(),
                recipients: vec![Recipient {
                    address: address.clone(),
                    amount: u64::MAX,
                }],
            },
            public_key: Some(pub_key_1.to_owned()),
            signature: None,
        };

        // expected wrong signature, signature is None.
        assert_eq!(transaction_1.verify_signature().is_err(), true);
        assert_eq!(transaction_1.verify_signature().is_ok(), false);

        let to_sign_1 = transaction_1.sign_bytes();
        let signature_1 = keypair_1.try_sign(&to_sign_1).unwrap();

        let transaction_to_validate_1 = Transaction {
            payload: Payload {
                sequence: 0,
                sender: address.clone(),
                recipients: vec![Recipient {
                    address: address.clone(),
                    amount: u64::MAX,
                }],
            },
            public_key: Some(pub_key.to_owned()),
            signature: Some(signature_1),
        };

        let signature_correct = keypair_1.try_sign(&to_sign_1).unwrap();

        // expected error, wrong public key.
        assert_eq!(transaction_to_validate_1.verify_signature().is_err(), true);
        assert_eq!(transaction_to_validate_1.verify_signature().is_ok(), false);

        let transaction_to_validate_2 = Transaction {
            payload: Payload {
                sequence: 0,
                sender: address.clone(),
                recipients: vec![Recipient {
                    address: address.clone(),
                    amount: u64::MAX,
                }],
            },
            public_key: Some(pub_key_1.to_owned()),
            signature: Some(signature_correct),
        };

        // expected correct signature.
        assert_eq!(transaction_to_validate_2.verify_signature().is_err(), false);
        assert_eq!(transaction_to_validate_2.verify_signature().is_ok(), true);

        let rs_serialize = Transaction::from(transaction_to_validate_2.to_owned()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(
            rs_serialize,
            Transaction::try_from(rs_serialize.to_owned()).unwrap()
        );
    }

    #[test]
    fn transaction_test_accept_multi_recipientss() {
        let sender_keypair = Keypair::new();
        let sender_pub_key = sender_keypair.public();

        let sender_address = Address::new(Default::default());
        let recipients_address1 = Address::new(Default::default());
        let recipients_address2 = Address::new(Default::default());
        let recipients_address3 = Address::new(Default::default());
        let recipients_address4 = Address::new(Default::default());
        let recipients_address5 = Address::new(Default::default());
        let recipients_address6 = Address::new(Default::default());
        let recipients_address7 = Address::new(Default::default());
        let recipients_address8 = Address::new(Default::default());
        let recipients_address9 = Address::new(Default::default());
        let recipients_address10 = Address::new(Default::default());

        //initial the tx_message
        let mut transaction_message = Transaction::new(sender_address, recipients_address1, 1, 0);

        //add recipients
        transaction_message.add_recipient(recipients_address2, 1);
        transaction_message.add_recipient(recipients_address3, 2);
        transaction_message.add_recipient(recipients_address4, 3);
        transaction_message.add_recipient(recipients_address5, 0);
        transaction_message.add_recipient(recipients_address6, 0);
        transaction_message.add_recipient(recipients_address7, 0);
        transaction_message.add_recipient(recipients_address8, 0);
        transaction_message.add_recipient(recipients_address9, 1);
        transaction_message.add_recipient(recipients_address10, 100);
        assert_eq!(transaction_message.payload.recipients.len() > 0, true);

        //initial with sender-key
        transaction_message.public_key = Some(sender_pub_key.to_owned());
        transaction_message.signature = None;

        //update the tx_message with sender-signature
        transaction_message.add_signature(sender_keypair);
        //verify_signature this tx message whether is CORRECTs
        assert_eq!(transaction_message.verify_signature().is_ok(), true);

        //get the total of recipients amount
        let total = transaction_message.total_amount();
        assert_eq!(total > 1, true);

        //hashing message
        let hs = transaction_message.hash();
        assert_eq!(hs.is_zero(), false);

        let rs_serialize = Transaction::from(transaction_message.to_owned()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(
            rs_serialize,
            Transaction::try_from(rs_serialize.to_owned()).unwrap()
        );
    }

    #[test]
    fn transaction_test_accept_sender_is_not_recipients() {
        let sender_keypair = Keypair::new();
        let sender_pub_key = sender_keypair.public();

        let sender_address = Address::new(Default::default());
        let recipients_address = Address::new(Default::default());

        //initial the tx_message
        let mut transaction_accept_different_sender_recipients =
            Transaction::new(sender_address, recipients_address, 99, 0);

        //initial with sender-key
        transaction_accept_different_sender_recipients.public_key = Some(sender_pub_key.to_owned());
        transaction_accept_different_sender_recipients.signature = None;

        //update the tx_message with sender-signature
        transaction_accept_different_sender_recipients.add_signature(sender_keypair);

        //expected correct signature.
        assert_eq!(
            transaction_accept_different_sender_recipients
                .verify_signature()
                .is_err(),
            false
        );
        assert_eq!(
            transaction_accept_different_sender_recipients
                .verify_signature()
                .is_ok(),
            true
        );

        let rs_serialize =
            Transaction::from(transaction_accept_different_sender_recipients.to_owned()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(
            rs_serialize,
            Transaction::try_from(rs_serialize.to_owned()).unwrap()
        );
    }

    #[test]
    fn transaction_test_not_accept_zero_amount() {
        let sender_keypair = Keypair::new();
        let sender_pub_key = sender_keypair.public();

        let sender_address = Address::new(Default::default());
        let recipients_address = Address::new(Default::default());

        //initial the tx_message
        let mut transaction_test_not_accept_zero_amount =
            Transaction::new(sender_address, recipients_address, 0, 0);

        //initial with sender-key
        transaction_test_not_accept_zero_amount.public_key = Some(sender_pub_key.to_owned());
        transaction_test_not_accept_zero_amount.signature = None;

        //update the tx_message with sender-signature
        transaction_test_not_accept_zero_amount.add_signature(sender_keypair);

        //expected correct return.
        assert_eq!(
            transaction_test_not_accept_zero_amount
                .verify_basic()
                .is_err(),
            true
        );

        let rs_serialize = Transaction::from(transaction_test_not_accept_zero_amount.to_owned()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(
            rs_serialize,
            Transaction::try_from(rs_serialize.to_owned()).unwrap()
        );
    }

    #[test]
    fn transaction_test_sender_with_invalid_signature() {
        let keypair1 = Keypair::new();
        let pub_key1 = keypair1.public().to_owned(); // pub-key1

        let sender_addr = Address::from(keypair1.public().to_owned());

        let receiver_addr = Address::from_str("1e33bb6d5382098474331580b8d137705b0333b1").unwrap();
        let receiver_amt = 99;
        let mut tx = Transaction::new(sender_addr, receiver_addr, receiver_amt, 0);

        let keypair2 = Keypair::new();
        let pub_key2 = keypair2.public().to_owned(); // pub-key2

        assert_eq!(pub_key1.ne(&pub_key2), true);

        let to_sign = tx.sign_bytes();
        let signature = keypair2.try_sign(&to_sign).unwrap();
        tx.public_key = Some(pub_key1); // set as pub-key1
        tx.signature = Some(signature); // sign signature for tx-mesage using pub-key2

        assert_eq!(tx.verify_basic().is_ok(), true);
        assert_eq!(tx.verify_signature().is_err(), true);

        let rs_serialize = Transaction::from(tx.to_owned()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(
            rs_serialize,
            Transaction::try_from(rs_serialize.to_owned()).unwrap()
        );
    }

    #[test]
    fn transaction_test_sender_with_valid_signature() {
        let keypair1 = Keypair::new();
        let pub_key1 = keypair1.public().to_owned(); // pub-key1

        let sender_addr = Address::from(keypair1.public().to_owned());

        let receiver_addr = Address::from_str("1e33bb6d5382098474331580b8d137705b0333b1").unwrap();
        let receiver_amt = 16;
        let mut tx = Transaction::new(sender_addr, receiver_addr, receiver_amt, 0);

        let to_sign = tx.sign_bytes();
        let signature = keypair1.try_sign(&to_sign).unwrap();
        tx.public_key = Some(pub_key1); // set as pub-key1
        tx.signature = Some(signature); // sign signature for tx-mesage using pub-key1

        assert_eq!(tx.verify_basic().is_ok(), true);
        assert_eq!(tx.verify_signature().is_ok(), true);

        let rs_serialize = Transaction::from(tx.to_owned()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(
            rs_serialize,
            Transaction::try_from(rs_serialize.to_owned()).unwrap()
        );
    }
}
