use crate::address::Address;
use error::error::{MgError, MgResult};
use hash::hash::hashing;
use primitive_types::H256;
use rlp_derive::{RlpDecodable, RlpEncodable};
use serde::{Deserialize, Serialize};
use std::convert::Into;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize, RlpDecodable, RlpEncodable)]
pub struct Account {
    pub address: Address,
    pub balance: u64,
    pub sequence: u64,
    pub flag: bool,
}

impl Account {
    pub fn new(addr: &Address, bal: u64, seq: u64, flag: bool) -> Account {
        Account {
            address: addr.clone(),
            balance: bal,
            sequence: seq,
            flag: flag,
        }
    }

    pub fn decode(data: Vec<u8>) -> MgResult<Self> {
        rlp::decode(&data).map_err(|_| MgError::ParsingError)
    }

    pub fn encode(&self) -> Vec<u8> {
        rlp::encode(self)
    }

    /// return the account is whitelisted.
    pub fn whitelist_address(&self) -> &bool {
        &self.flag
    }

    /// Account hash
    pub fn hash(&self) -> H256 {
        return hashing(self);
    }

    pub fn verify(&self) -> Result<(), MgError> {
        if self.address.as_bytes().len() < 20 {
            return Err(MgError::InvalidAccount(
                "The account address is invalid".to_owned(),
            ));
        }
        if self.flag == false {
            return Err(MgError::InvalidAccount(
                "The account is not whitelisted".to_owned(),
            ));
        }

        Ok(())
    }
}

impl Into<Vec<u8>> for &Account {
    fn into(self) -> Vec<u8> {
        self.encode()
    }
}

#[cfg(test)]
#[path = "./account_test.rs"]
mod account_test;
