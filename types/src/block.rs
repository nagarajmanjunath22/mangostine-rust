use crate::transaction::Transaction;
use hash::hash::hashing;
use chrono::{DateTime, NaiveDateTime, Utc};
use merkle_tree::trie;
use primitive_types::H256;
pub mod header;
pub use crate::block::header::{Header, Version};
use error::error::{MgError, MgResult};
use rlp_derive::{RlpDecodable, RlpEncodable};
use std::convert::Into;

#[derive(Clone, Debug, PartialEq, RlpEncodable, RlpDecodable)]
pub struct Block {
    pub header: Header,
    pub transactions: Vec<Transaction>,
}

impl Block {
    pub fn new(header: Header, transactions: Vec<Transaction>) -> Block {
        let mut leafs = Vec::new();
        for tx in &transactions {
            let hash = tx.hash();
            let bytes: Vec<u8> = tx.into();
            leafs.push((hash, bytes));
        }
        let root = trie::trie_root_no_extension(leafs);
        let header = Header::new(
            header.block_number,
            header.prev_block_hash,
            header.app_hash,
            Utc::now(),
            root,
            header.chain_id,
            header.version,
        );
        Block {
            header,
            transactions,
        }
    }

    pub fn decode(data: Vec<u8>) -> MgResult<Self> {
        rlp::decode(&data).map_err(|_| MgError::ParsingError)
    }

    pub fn encode(&self) -> Vec<u8> {
        rlp::encode(self)
    }

    pub fn block_time(&self) -> DateTime<Utc> {
        let native = NaiveDateTime::from_timestamp(self.header.block_time_unix as i64, 0);
        DateTime::from_utc(native, Utc)
    }

    pub fn hash(&self) -> H256 {
        return hashing(self);
    }

    // verify the block
    pub fn verify(&self) -> Result<(), MgError> {
        //verify the header
        if let Err(e) = self.header.verify() {
            return Err(MgError::InvalidBlock(e.to_string().to_owned()));
        }
        //verify transaction
        for tx in &self.transactions {
            if let Err(e) = tx.verify_basic() {
                return Err(MgError::InvalidBlock(e.to_string().to_owned()));
            }
        }

        Ok(())
    }
}

impl Into<Vec<u8>> for &Block {
    fn into(self) -> Vec<u8> {
        self.encode()
    }
}

#[cfg(test)]
#[path = "./block_test.rs"]
mod block_test;
