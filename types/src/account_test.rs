#[cfg(test)]
mod account_test {

    use super::super::*;
    use Account;
    use Address;
    use std::str::FromStr;
    use std::convert::{From};

    #[test]
    fn account_single_creation(){
        let addr = Address::from_str("1a77bb6d5382098474331580b8d137705b0271b1").unwrap();
        let bal = 999;
        let seq = 1;
        let is_kyc = false;
        let account = Account::new(&addr, bal, seq, is_kyc);

        assert_eq!(account.verify().is_err(), true);
        assert_eq!(account.balance.to_owned() > 0, true);

        let hash = account.hash();
        assert_eq!(hash.is_zero(), false);

        let rs_serialize = Account::from(account.to_owned());                        // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize, Account::try_from(rs_serialize.to_owned()).unwrap());

    }

    #[test]
    fn account_multiple_creation() {
        let account1 = Account::new(
            &Address::from_str("1a77bb6d5382098474331580b8d137705b0271b1").unwrap(),
            999,
            1,
            false,
        );
        assert_eq!(account1.verify().is_err(), true);
        assert_eq!(account1.whitelist_address().to_owned(), false);

        let rs_serialize_account1 = Account::from(account1.to_owned());                        // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize_account1, Account::try_from(rs_serialize_account1.to_owned()).unwrap());

        let account2 = Account::new(
            &Address::new(Default::default()),
            17,
            1,
            true,
        );
        assert_eq!(account2.verify().is_err(), false);
        assert_eq!(account2.balance.to_owned() > 0, true);

        let rs_serialize_account2 = Account::from(account2.to_owned());                        // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize_account2, Account::try_from(rs_serialize_account2.to_owned()).unwrap());

        let account3 = Account::new(
            &Address::from_str("2b77bb6d5382098474331580b8d137705b0271b2").unwrap(),
            0,
            1,
            true,
        );
        assert_eq!(account3.verify().is_err(), false);
        assert_eq!(account3.sequence.to_owned() > 0, true);
        assert_eq!(account3.whitelist_address().to_owned(), true);

        let rs_serialize_account3 = Account::from(account3.to_owned());                        // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize_account3, Account::try_from(rs_serialize_account3.to_owned()).unwrap());

    }
}
