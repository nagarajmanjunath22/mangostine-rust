#[cfg(test)]
mod tests {
    use crate::address::Address;
    use crate::block::header;
    use crate::block::Block;
    use crate::transaction::Transaction;
    use chrono::Utc;
    use header::{Header, Version};
    use hex;
    use primitive_types::{H256, U256};
    use std::convert::{From, TryFrom};
    use std::str::FromStr;

    use crypto::keypair::Keypair;
    use error::error::MgError;

    /// create the  default header
    pub fn create_header() -> Header {
        let block_number = U256::from_dec_str("0").unwrap();
        let chain_id: String = "mangostine".to_owned();
        let version = Version::default();
        Header::new(
            block_number,
            H256::random(),
            H256::random(),
            Utc::now(),
            H256::zero(),
            chain_id,
            version,
        )
    }
    /// create the transactions based on the number of input
    pub fn create_transaction(n: i32) -> Vec<Transaction> {
        let mut transactions: Vec<Transaction> = Vec::new();
        for _ in 0..n {
            let add = Address::from_str("1a77bb6d5382098474331580b8d137705b0271b1").unwrap();
            let add1 = Address::from_str("F3349DBE0E4B1ECD85899D145E2A3A534C8311B1").unwrap();
            let signer = add;
            let receiver = add1;
            let tx = Transaction::new(signer, receiver, u64::MAX, 0);
            transactions.push(tx);
        }
        transactions
    }
    /// Contains two transaction with same merkle root hash and merkle root hash matches
    #[test]
    fn block_tx_merkle() {
        //get the block header
        let block_header = create_header();
        //get the transaction
        let transactions = create_transaction(2);
        // create the block
        let block_0 = Block::new(block_header, transactions);
        // get the merkle root hash
        let merkel_root = block_0.header._tx_root_hash;
        println!("the value in merkle_root {:?}", merkel_root);
        let val = hex::decode("b7fea69a027261c059d1f42cdb9bad8f6ecc42df0695bf71b50b9abf3952727b")
            .unwrap();
        let hash = merkel_root.as_bytes();
        assert_eq!(hash, val);
    }

    /// Contains three transaction different merkle root hash does not match
    #[test]
    #[should_panic(
        expected = "Number of Transactions in block must be same to create same merkle root hash"
    )]
    fn block_check_tx_merkle() {
        //get the block header
        let block_header = create_header();
        //get the transaction
        let transactions = create_transaction(3);
        // create the block
        let block_0 = Block::new(block_header, transactions);
        // get the merkle root hash
        let merkel_root = block_0.header._tx_root_hash;
        let val = hex::decode("b7fea69a027261c059d1f42cdb9bad8f6ecc42df0695bf71b50b9abf3952727b")
            .unwrap();
        let hash = merkel_root.as_bytes();
        assert_eq!(hash, val);
    }

    /// Generate the genesis block
    #[test]
    fn block_generate_genesis_block() {
        // genesis-transaction
        let genesis_sender = Address::from_str("2b77bb6d5382098474331580b8d137705b0271b2").unwrap();
        let genesis_recipient =
            Address::from_str("C3349DBE0E4B1ECD85899D145E2A3A534C8311C3").unwrap();
        let genesis_recipient_amount = 1;
        let genesis_transaction = Transaction::new(
            genesis_sender,
            genesis_recipient,
            genesis_recipient_amount,
            0,
        );
        let block_transactions = vec![];

        // genesis-block
        let header_block_number = U256::zero();
        let header_prev_block_hash = H256::zero();
        let header_app_hash = H256::zero();
        let header_tx_root_hash = H256([0; 32]);
        let header_block_timestamp = Utc::now();
        let header_chain_id = "mangostine".to_owned();
        let header_version = Version::default();

        let block_header = Header::new(
            header_block_number,
            header_prev_block_hash,
            header_app_hash,
            header_block_timestamp,
            header_tx_root_hash,
            header_chain_id,
            header_version,
        );

        // compile the block
        let mut genesis_block = Block::new(block_header, block_transactions);
        genesis_block
            .transactions
            .append(&mut vec![genesis_transaction]);
        genesis_block.header.block_number = U256::from_str("1").unwrap(); // is genesis-block
        assert_eq!(genesis_block.header.block_number.is_zero(), false);

        // generate the Hash-value of this Block & set to 'tx_root_hash'
        genesis_block.header._tx_root_hash = genesis_block.block_hash(); //current_hash;
        assert_eq!(genesis_block.header._tx_root_hash.is_zero(), false);

        // set app_hash
        genesis_block.header.app_hash = H256::random();
        assert_eq!(genesis_block.header.app_hash.is_zero(), false);

        // set current timestamp
        genesis_block.header.block_time_unix = genesis_block.block_time().timestamp();

        let rs_serialize = Block::from(genesis_block.clone()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize.clone(), Block::try_from(rs_serialize).unwrap());
    }

    /// Generate the all blocks
    #[test]
    fn block_generate_all_blocks() {
        let genesis_block = block_get_genesis_block();

        // compile : non-genesis-block for current transaction
        let header_block_number = U256::zero();
        let header_prev_block_hash = H256([0; 32]);
        let header_app_hash = H256::zero();
        let header_tx_root_hash = H256([0; 32]);
        let header_block_timestamp = Utc::now();
        let header_chain_id = "mangostine".to_owned();
        let header_version = Version::default();
        let block_transactions = vec![];
        let block_header = Header::new(
            header_block_number,
            header_prev_block_hash,
            header_app_hash,
            header_block_timestamp,
            header_tx_root_hash,
            header_chain_id,
            header_version,
        );

        let mut next_block = Block::new(block_header, block_transactions);

        let new_sender1 = Address::from_str("1a77bb6d5382098474331580b8d137705b0271b1").unwrap();
        let new_recipient1 = Address::from_str("F3349DBE0E4B1ECD85899D145E2A3A534C8311B1").unwrap();
        let new_recipient_amount1 = 111;
        let tx1 = new_transaction(new_sender1, new_recipient1, new_recipient_amount1);
        next_block.transactions.append(&mut vec![tx1]);

        let new_sender2 = Address::from_str("2a77bb6d5382098474331580b8d137705b0271b2").unwrap();
        let new_recipient2 = Address::from_str("F3349DBE0E4B1ECD85899D145E2A3A534C8311B2").unwrap();
        let new_recipient_amount2 = 22;
        let tx2 = new_transaction(new_sender2, new_recipient2, new_recipient_amount2);
        next_block.transactions.append(&mut vec![tx2]);

        let new_sender3 = Address::from_str("3a77bb6d5382098474331580b8d137705b0271b3").unwrap();
        let new_recipient3 = Address::from_str("F3349DBE0E4B1ECD85899D145E2A3A534C8311B3").unwrap();
        let new_recipient_amount3 = 3;
        let tx3 = new_transaction(new_sender3, new_recipient3, new_recipient_amount3);
        next_block.transactions.append(&mut vec![tx3]);

        let new_sender4 = Address::from_str("4a77bb6d5382098474331580b8d137705b0271b4").unwrap();
        let new_recipient4 = Address::from_str("F3349DBE0E4B1ECD85899D145E2A3A534C8311B4").unwrap();
        let new_recipient_amount4 = 441;
        let tx4 = new_transaction(new_sender4, new_recipient4, new_recipient_amount4);
        next_block.transactions.append(&mut vec![tx4]);

        next_block.header.prev_block_hash = genesis_block.header._tx_root_hash;
        assert_eq!(next_block.header.prev_block_hash.is_zero(), false);

        next_block.header.block_number = U256::from_dec_str("2").unwrap(); // 2nd-block
        assert_eq!(next_block.header.block_number.is_zero(), false);
        // generate the Hash-value of this Block & set to 'tx_root_hash'
        next_block.header._tx_root_hash = next_block.block_hash(); //current_hash;
        assert_eq!(next_block.header._tx_root_hash.is_zero(), false);
        //set app_hash
        next_block.header.app_hash = H256::random();
        assert_eq!(next_block.header.app_hash.is_zero(), false);

        next_block.header.block_time_unix = next_block.block_time().timestamp();
        //1. verify the block-header is VALID before added-into the CHAIN
        let error = next_block.header.verify();
        assert_eq!(error.is_err(), false);
        //2. verify the block (Header and Trx-details) is VALID before added-into the CHAIN
        assert_eq!(next_block.verify().is_err(), false);
        //hashing block-message before send-out
        let hash = next_block.block_hash();
        assert_eq!(hash.is_zero(), false);

        let rs_serialize = Block::from(next_block.clone()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize, Block::try_from(rs_serialize.clone()).unwrap());
    }

    // sub-method
    fn new_transaction(
        tx_sender: Address,
        tx_recipient: Address,
        recipient_amount: u64,
    ) -> Transaction {
        let tx_message = Transaction::new(tx_sender, tx_recipient, recipient_amount, 0);

        //1. verify the transaction base on keypair and signature
        let tx = transaction_verify(tx_message).unwrap();

        //1. verify the transaction base on keypair and signature
        let trx = tx.verify_signature();
        assert_eq!(trx.is_err(), false);

        return tx;
    }

    //sub-method
    fn transaction_verify(trx: Transaction) -> Result<Transaction, MgError> {
        let sender_keypair = Keypair::new();
        let sender_pub_key = sender_keypair.public();

        let mut tx = trx;

        //initial with sender-key
        tx.public_key = Some(sender_pub_key.to_owned());
        tx.signature = None;

        //update the tx_message with sender-signature
        tx.add_signature(sender_keypair);

        let rs = tx.verify_signature();
        assert_eq!(rs.is_err(), false);

        //7. get the total of receiver amount
        let tot = tx.total_amount();
        assert_eq!(tot > 0, true);

        //8. hashing message before send-out
        let hash = tx.hash();
        assert_eq!(hash.is_zero(), false);

        return Ok(tx);
    }

    // sub-method
    fn block_get_genesis_block() -> Block {
        // genesis-transaction
        let genesis_sender = Address::from_str("2b77bb6d5382098474331580b8d137705b0271b2").unwrap();
        let genesis_recipient =
            Address::from_str("C3349DBE0E4B1ECD85899D145E2A3A534C8311C3").unwrap();
        let genesis_recipient_amount = 1;
        let genesis_transaction = Transaction::new(
            genesis_sender,
            genesis_recipient,
            genesis_recipient_amount,
            0,
        );
        let block_transactions = vec![];

        // genesis-block
        let header_block_number = U256::zero();
        let header_prev_block_hash = H256::zero();
        let header_app_hash = H256::zero();
        let header_tx_root_hash = H256([0; 32]);
        let header_block_timestamp = Utc::now();
        let header_chain_id = "mangostine".to_owned();
        let header_version = Version::default();

        let block_header = Header::new(
            header_block_number,
            header_prev_block_hash,
            header_app_hash,
            header_block_timestamp,
            header_tx_root_hash,
            header_chain_id,
            header_version,
        );

        // compile the block
        let mut genesis_block = Block::new(block_header, block_transactions);
        genesis_block
            .transactions
            .append(&mut vec![genesis_transaction]);
        genesis_block.header.block_number = U256::from_str("1").unwrap(); // is genesis-block
        assert_eq!(genesis_block.header.block_number.is_zero(), false);

        // generate the Hash-value of this Block & set to 'tx_root_hash'
        genesis_block.header._tx_root_hash = genesis_block.block_hash(); //current_hash;
        assert_eq!(genesis_block.header._tx_root_hash.is_zero(), false);

        // set app_hash
        genesis_block.header.app_hash = H256::random();
        assert_eq!(genesis_block.header.app_hash.is_zero(), false);

        // set current timestamp
        genesis_block.header.block_time_unix = genesis_block.block_time().timestamp();

        let rs_serialize = Block::from(genesis_block.clone()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize, Block::try_from(rs_serialize.clone()).unwrap());

        return genesis_block;
    }
}
