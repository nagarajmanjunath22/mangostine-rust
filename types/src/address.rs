use ed25519_dalek::PublicKey;
use error::error::MgError;
use primitive_types::H160;
use hex;

/// Account Address
pub type Address = H160;

pub fn address_from_public_key(pb: PublicKey) -> Address {
    let mut hb = blake3::Hasher::new();
    hb.update(pb.as_bytes());
    let hash = hb.finalize();
    let mut result = Address::zero();
    result.as_bytes_mut().copy_from_slice(&hash.as_bytes()[12..]);
    result
}

pub fn address_from_string(s: &str) -> Result<Address, MgError> {
    // Accept either upper or lower case hex
    let bytes = hex::decode(s)
        .map_err(|_| MgError::InvalidAddress)?;

    if bytes.len() != 20 {
        return Err(MgError::InvalidAddress);
    }

    let mut result = Address::zero();
    result.as_bytes_mut().copy_from_slice(&bytes[12..]);
    Ok(result)
}

#[cfg(test)]
#[path = "./address_test.rs"]
mod address_test;
