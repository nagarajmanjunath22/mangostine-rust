pub mod account;
pub mod address;
pub mod block;
pub mod transaction;
pub mod address_test;
pub mod block_test;

pub type UnixTimestamp = u64;
pub type NodeId = u64;
