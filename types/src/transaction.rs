use crate::address::Address;
use bincode::{deserialize, serialize};
use crypto::keypair::Keypair;
use ed25519_dalek::{PublicKey, Signature};
use error::error::{MgError, MgResult};
use hash::hash::hashing;
use primitive_types::H256;
use rlp::{Decodable, Encodable};
use rlp_derive::{RlpDecodable, RlpEncodable};
use serde::{Deserialize, Serialize};
use std::convert::Into;

#[derive(Debug, Eq, Clone, PartialEq, Serialize, Deserialize, RlpEncodable, RlpDecodable)]
pub struct Payload {
    pub sender: Address,
    pub recipients: Vec<Recipient>,
    pub sequence: u64,
}

#[derive(Debug, Eq, Clone, PartialEq, Serialize, Deserialize, RlpEncodable, RlpDecodable)]
pub struct Recipient {
    pub address: Address,
    pub amount: u64,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Transaction {
    pub payload: Payload,
    pub public_key: Option<PublicKey>,
    pub signature: Option<Signature>,
}

impl Transaction {
    pub fn new(sender: Address, receiver: Address, amt: u64, seq: u64) -> Self {
        Transaction {
            payload: Payload {
                sequence: seq,
                sender,
                recipients: vec![Recipient {
                    address: receiver,
                    amount: amt,
                }],
            },
            public_key: None,
            signature: None,
        }
    }

    pub fn add_recipient(&mut self, address: Address, amt: u64) {
        &self.payload.recipients.push(Recipient {
            address: address,
            amount: amt,
        });
    }

    pub fn hash(&self) -> H256 {
        return hashing(self);
    }

    pub fn new_multi_recipients(
        sender: Address,
        recipients: Vec<Recipient>,
        seq: u64,
        public_key: Option<PublicKey>,
        signature: Option<Signature>,
    ) -> Self {
        Transaction {
            payload: Payload {
                sequence: seq,
                sender,
                recipients,
            },
            public_key,
            signature,
        }
    }

    pub fn decode(data: Vec<u8>) -> MgResult<Self> {
        rlp::decode(&data).map_err(|_| MgError::ParsingError)
    }

    pub fn encode(&self) -> Vec<u8> {
        rlp::encode(self)
    }

    pub fn total_amount(&self) -> u64 {
        let mut total_amount = 0;
        for i in &self.payload.recipients {
            total_amount = total_amount + i.amount;
        }

        return total_amount;
    }

    pub fn add_signature(&mut self, sender_keypair: Keypair) {
        let to_sign_message = &self.sign_bytes();
        self.signature = Some(sender_keypair.try_sign(&to_sign_message).unwrap());
    }

    pub fn verify_basic(&self) -> std::result::Result<(), MgError> {
        // TODO :: Add verify_basic method to all primitives and check them here
        if self.public_key.is_none() {
            return Err(MgError::InvalidTx("Public key not found.".to_owned()));
        }

        if self.signature.is_none() {
            return Err(MgError::InvalidTx("Signature not found.".to_owned()));
        }

        if self.payload.recipients.is_empty() {
            return Err(MgError::InvalidTx("Receiver not found.".to_owned()));
        }

        if self.payload.recipients.len() > 0 {
            for r in &self.payload.recipients {
                if r.amount <= 0 {
                    return Err(MgError::InvalidTx(
                        "Amount cannot be 0 or lower than 0.".to_owned(),
                    ));
                }
            }
        }
        Ok(())
    }

    pub fn verify_signature(&self) -> std::result::Result<(), MgError> {
        if let Some(pub_key) = &self.public_key {
            if let Some(sig) = &self.signature {
                if let Err(e) = pub_key.verify_strict(&self.sign_bytes(), sig) {
                    return Err(MgError::InvalidTx(format!("Invalid Signature. {}", e)));
                } else {
                    Ok(())
                }
            } else {
                return Err(MgError::InvalidTx("Signature not found.".to_owned()));
            }
        } else {
            return Err(MgError::InvalidTx("Public key not found.".to_owned()));
        }
    }

    pub fn sign_bytes(&self) -> Vec<u8> {
        rlp::encode(&self.payload)
    }
}

impl Encodable for Transaction {
    fn rlp_append(&self, s: &mut rlp::RlpStream) {
        s.append(&self.payload);
        let public_key_data = serialize(&self.public_key).unwrap();
        s.append_raw(&public_key_data[..], public_key_data.len());
        let signature_data = serialize(&self.signature).unwrap();
        s.append_raw(&signature_data[..], signature_data.len());
    }
}

impl Decodable for Transaction {
    fn decode(rlp: &rlp::Rlp<'_>) -> std::result::Result<Self, rlp::DecoderError> {
        if !rlp.is_list() && rlp.size() != 3 {
            return Err(rlp::DecoderError::RlpIncorrectListLen);
        }

        let payload: Payload = rlp.at(0)?.as_val()?;
        let public_key_data = rlp.at(1)?.as_raw();
        let signature_data = rlp.at(2)?.as_raw();

        let public_key: PublicKey = deserialize(&public_key_data).unwrap();
        let signature: Signature = deserialize(&signature_data).unwrap();

        Ok(Transaction {
            payload: payload,
            public_key: Some(public_key),
            signature: Some(signature),
        })
    }
}

impl Into<Vec<u8>> for &Transaction {
    fn into(self) -> Vec<u8> {
        self.encode()
    }
}

#[cfg(test)]
#[path = "./transaction_test.rs"]
mod transaction_test;
