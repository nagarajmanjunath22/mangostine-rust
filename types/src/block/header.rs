use crate::UnixTimestamp;
use chrono::{DateTime, NaiveDateTime, Utc};
use error::error::{MgError, MgResult};
use primitive_types::H256;
use rlp_derive::{RlpDecodable, RlpEncodable};

#[derive(Debug, Clone, PartialEq, RlpEncodable, RlpDecodable)]
pub struct Header {
    pub chain_id: String,
    pub version: Version,
    pub block_number: u64,
    pub prev_block_hash: H256,
    pub app_hash: H256,
    pub block_time_unix: UnixTimestamp,
    pub _tx_root_hash: H256,
}

impl Header {
    pub fn new(
        block_number: u64,
        previous_block_hash: H256,
        app_hash: H256,
        block_time: DateTime<Utc>,
        _tx_root_hash: H256,
        chain_id: String,
        version: Version,
    ) -> Self {
        Header {
            block_number: block_number,
            prev_block_hash: previous_block_hash,
            app_hash: app_hash,
            block_time_unix: block_time.timestamp()  as u64,
            _tx_root_hash: H256::zero(),
            chain_id: chain_id,
            version: version,
        }
    }

    pub fn decode(data: Vec<u8>) -> MgResult<Self> {
        rlp::decode(&data).map_err(|_| MgError::ParsingError)
    }

    pub fn encode(&self) -> Vec<u8> {
        rlp::encode(self)
    }

    pub fn block_time(&self) -> DateTime<Utc> {
        let naive = NaiveDateTime::from_timestamp(self.block_time_unix  as i64, 0);
        DateTime::from_utc(naive, Utc)
    }

    //verify block header
    pub fn verify(&self) -> Result<(), MgError> {
        let chain_id = self.chain_id.clone();

        if self.app_hash.is_zero() {
            return Err(MgError::InvalidHeader(
                "The app_hash must contains value of 32 bytes".to_owned(),
            ));
        }

        if self.block_number == 0 {
            return Err(MgError::InvalidHeader(
                "The block_number is invalid".to_owned(),
            ));
        }

        if chain_id.is_empty() {
            return Err(MgError::InvalidHeader(
                "The chain_id must not be empty".to_owned(),
            ));
        }

        if self.prev_block_hash.is_zero() {
            return Err(MgError::InvalidHeader(
                "The prev_block_hash must contains value of 32 bytes".to_owned(),
            ));
        }

        if self._tx_root_hash.is_zero() {
            return Err(MgError::InvalidHeader(
                "The tx_root_hash must contains value of 32 bytes".to_owned(),
            ));
        }
        Ok(())
    }


}

#[derive(Debug, Clone, PartialEq, RlpDecodable, RlpEncodable)]
pub struct Version {
    /// Block version
    pub block: u64,
    /// App version
    pub app: u64,
}

impl Default for Version {
    fn default() -> Self {
        Self { block: 1, app: 1 }
    }
}

#[cfg(test)]
#[path = "./header_test.rs"]
mod header_test;
