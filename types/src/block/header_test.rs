mod test {
    use super::super::*;
    use chrono::{DateTime, Utc};
    use primitive_types::{H256, U256};
    use std::convert::{From};

    /// create the  default header
    pub fn create_header(
        block_number: U256,
        previous_hash: H256,
        app_hash: H256,
        block_time: DateTime<Utc>,
        tx_root_hash: H256,
        chain_id: String,
        version: Version,
    ) -> Header {
        Header::new(
            block_number,
            previous_hash,
            app_hash,
            block_time,
            tx_root_hash,
            chain_id,
            version,
        )
    }

    #[test]
    #[should_panic(expected = "Block number cannot be zero")]
    pub fn header_verify_blocknumber() {
        let block_number = U256::zero();
        let chain_id: String = "mangostine".to_owned();
        let version = Version::default();
        let header = create_header(
            block_number,
            H256::random(),
            H256::random(),
            Utc::now(),
            H256::random(),
            chain_id,
            version,
        );
        let rs_serialize = Header::from(header.to_owned());                        // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize, Header::try_from(rs_serialize.to_owned()).unwrap());

        let error = header.verify();
        assert_eq!(error.is_err(), true);
        println!("the error is {:?}", error);

    }

    #[test]
    #[should_panic(expected = "previous_hash cannot be empty/null")]
    pub fn header_verify_previous_hash() {
        let block_number = U256::one();
        let chain_id: String = "mangostine".to_owned();
        let version = Version::default();
        let header = create_header(
            block_number,
            H256::zero(),
            H256::random(),
            Utc::now(),
            H256::random(),
            chain_id,
            version,
        );
        let rs_serialize = Header::from(header.to_owned());                        // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize, Header::try_from(rs_serialize.to_owned()).unwrap());

        let error = header.verify();
        assert_eq!(error.is_err(), true);
        println!("the error is {:?}", error);

    }

    #[test]
    #[should_panic(expected = "app_hash cannot be empty/null")]
    pub fn header_verify_app_hash() {
        let block_number = U256::one();
        let chain_id: String = "mangostine".to_owned();
        let version = Version::default();
        let header = create_header(
            block_number,
            H256::random(),
            H256::zero(),
            Utc::now(),
            H256::random(),
            chain_id,
            version,
        );
        let rs_serialize = Header::from(header.to_owned());                        // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize, Header::try_from(rs_serialize.to_owned()).unwrap());

        let error = header.verify();
        assert_eq!(error.is_err(), true);
        println!("the error is {:?}", error);

    }

    #[test]
    #[should_panic(expected = "tx_root_hash cannot be empty/zero")]
    pub fn header_verify_tx_root_hash() {
        let block_number = U256::one();
        let chain_id: String = "mangostine".to_owned();
        let version = Version::default();
        let header = create_header(
            block_number,
            H256::random(),
            H256::random(),
            Utc::now(),
            H256::random(),
            chain_id,
            version,
        );
        let rs_serialize = Header::from(header.to_owned());                        // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize, Header::try_from(rs_serialize.to_owned()).unwrap());

        let error = header.verify();
        assert_eq!(error.is_err(), true);
        println!("the error is {:?}", error);

    }

    #[test]
    #[should_panic(expected = "chain_id cannot be empty/null")]
    pub fn header_verify_chain_id() {
        let block_number = U256::one();
        let chain_id = String::from("");
        let version = Version::default();
        let header = create_header(
            block_number,
            H256::random(),
            H256::random(),
            Utc::now(),
            H256::random(),
            chain_id,
            version,
        );
        let rs_serialize = Header::from(header.to_owned());                        // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(rs_serialize, Header::try_from(rs_serialize.to_owned()).unwrap());

        let error = header.verify();
        assert_eq!(error.is_err(), true);
        println!("the error is {:?}", error);

    }
}


