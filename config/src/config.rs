use consensus_raft::config::RaftConfig;
use mempool::Config as MempoolConfig;
use network::Config as NetworkConfig;
use grpc::Config as GrpcConfig;
use serde_derive::*;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Config {
    pub network: NetworkConfig,
    pub mempool: MempoolConfig,
    pub consensus: RaftConfig,
    pub grpc: GrpcConfig,

}

impl Default for Config {
    fn default() -> Self {
        Self {
            network: NetworkConfig::default(),
            mempool: MempoolConfig::default(),
            consensus: RaftConfig::default(),
            grpc: GrpcConfig::default(),
        }
    }
}
