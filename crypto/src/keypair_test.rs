#[cfg(test)]
mod tests {
    use super::super::*;

    #[test]
    fn keypair_test() {
        let keypair = Keypair::new();
        let keypair_bz = &keypair.to_bytes();
        let keypair_from_bz = Keypair::from_bytes(keypair_bz).unwrap();

        let secret_key = keypair.secret();
        let secret_bz = secret_key.as_bytes();
        assert_eq!(secret_bz.len(), 32);

        let pub_key = keypair.public();
        let pub_bz = pub_key.as_bytes();
        assert_eq!(pub_bz.len(), 32);

        // compare secret_key_1 with secret key, they both should be the same.
        let secret_key_1 = keypair_from_bz.secret();
        let secret_bz_1 = secret_key_1.as_bytes();
        assert_eq!(secret_bz_1, secret_bz);

        // compare pub_bz with pub_key_bz_1, they both should be the same.
        let pub_key_1 = keypair_from_bz.public();
        let pub_key_bz_1 = pub_key_1.as_bytes();
        assert_eq!(pub_key_bz_1, pub_bz);
    }

    #[test]
    fn keypair_test_generate_with_seed() {
        let keypair = Keypair::generate_with_seed();
        let mnemonic = keypair.1;
        let mnemonic_bz = mnemonic.as_bytes();
        let keypair_from_mnemonic_bz = keypair_from_seed(mnemonic_bz).unwrap();

        let secret_key = keypair.0.secret();
        let secret_bz = secret_key.as_bytes();
        assert_eq!(secret_bz.len(), 32);

        let pub_key = keypair.0.public();
        let pub_bz = pub_key.as_bytes();
        assert_eq!(pub_bz.len(), 32);

        // compare secret_key_1 with secret key, they both should be the same.
        let secret_key_1 = keypair_from_mnemonic_bz.secret();
        let secret_bz_1 = secret_key_1.as_bytes();
        assert_eq!(secret_bz_1, secret_bz);

        // compare pub_bz with pub_key_bz_1, they both should be the same.
        let pub_key_1 = keypair_from_mnemonic_bz.public();
        let pub_key_bz_1 = pub_key_1.as_bytes();
        assert_eq!(pub_key_bz_1, pub_bz);
    }
}
