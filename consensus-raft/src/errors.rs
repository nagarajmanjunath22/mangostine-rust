#[derive(Debug)]
pub enum RaftError {
    ConfigInvalid(String),
}
