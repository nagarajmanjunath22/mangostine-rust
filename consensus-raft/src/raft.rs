use crate::config::RaftConfig;
use crate::leader::NodeRole;
use crate::leader::RaftRole;
use crate::message::RaftMessage;
use crate::message::RaftMessageType;
use std::time::{SystemTime, UNIX_EPOCH};
use log::info;

pub type RaftNodeId = u64;
pub const RAFT_INVALID_NODE_ID: RaftNodeId = 0;
const RAFT_NODE_ID_CONST: RaftNodeId = 7777727;

pub struct RaftNode {
    node_id: RaftNodeId,
    role: RaftRole,
    tick_duration_ms: u64,
}

impl RaftNode {
    pub fn random_node_id() -> RaftNodeId {
        let now = SystemTime::now();
        let id = now.duration_since(UNIX_EPOCH).expect("Invalid system time");
        let s1 = id.as_secs();
        let s2 = id.subsec_nanos() as u64;
        ((s1 + s2) % RAFT_NODE_ID_CONST) * 1_000_000 + s2
    }

    pub fn new(config: &RaftConfig) -> Self {
        let mut node_id = config.node_id;
        if node_id == RAFT_INVALID_NODE_ID {
            node_id = Self::random_node_id();
        }

        if let Err(error) = config.validate() {
            panic!("Cannot instantiate raft node, err: {:?}", error);
        }

        info!("Creating new node id:{}", node_id);
        Self {
            node_id: node_id,
            role: RaftRole::new(config),
            tick_duration_ms: config.tick_duration_ms,
        }
    }

    pub fn node_id(&self) -> RaftNodeId {
        self.node_id
    }

    pub fn node_role(&self) -> NodeRole {
        self.role.current_state().clone()
    }

    pub fn current_leader(&self) -> RaftNodeId {
        self.role.leader_id()
    }

    pub fn tick_duration_ms(&self) -> u64 {
        self.tick_duration_ms
    }

    pub fn election_timeout(&self) -> usize {
        self.role.election_tick()
    }

    pub fn min_timeout(&self) -> usize {
        self.role.min_election_tick()
    }

    pub fn max_timeout(&self) -> usize {
        self.role.max_election_tick()
    }

    pub fn tick(&mut self) {
        self.role.tick();
    }

    pub fn role(&self) -> &RaftRole {
        &self.role
    }

    pub fn role_mut(&mut self) -> &mut RaftRole {
        &mut self.role
    }

    pub fn on_msg(&self, msg: RaftMessage) {
        match msg.msg_type() {
            RaftMessageType::MsgAppendEntries => {
                self.on_msg_append_entries(msg.payload());
            }
            RaftMessageType::MsgAppendEntriesResponse => {
                self.on_msg_append_entries_response(msg.payload());
            }
            RaftMessageType::MsgRequestVote => {
                self.on_msg_request_vote(msg.payload());
            }
            RaftMessageType::MsgRequestVoteResponse => {
                self.on_msg_request_vote_response(msg.payload());
            }
            RaftMessageType::MsgTransferLeader => {
                self.on_msg_transfer_leader(msg.payload());
            }
        }
    }

    fn on_msg_request_vote(&self, _payload: &Vec<u8>) {
    }

    fn on_msg_request_vote_response(&self, _payload: &Vec<u8>) {}

    fn on_msg_append_entries(&self, _payload: &Vec<u8>) {
        //self.role_mut().randomized_election_tick();
    }

    fn on_msg_append_entries_response(&self, _payload: &Vec<u8>) {}

    fn on_msg_transfer_leader(&self, _payload: &Vec<u8>) {}
}

#[cfg(test)]
#[path = "./raft_test.rs"]
mod raft_test;
