use crate::config::RaftConfig;
use crate::message::RaftMessageType;
use crate::raft::RaftNodeId;
use crate::raft::RAFT_INVALID_NODE_ID;
use fsm::fsm::*;
use fsm_derive::*;
use log::{debug, info};
use rand::distributions::{Distribution, Uniform};

#[derive(Debug, Hash, PartialEq, FsmTraitState)]
pub enum NodeRole {
    Follower,
    Candidate,
    Leader,
}

impl Default for NodeRole {
    fn default() -> NodeRole {
        NodeRole::Follower
    }
}

#[derive(Debug, Hash, PartialEq, FsmTraitEvent)]
pub enum NodeEvent {
    OnAppendEntries,
    OnElectionTimeout,
    OnVoteRequest,
    OnVoteReceived,
    OnVoteMajorityReceived,
    OnLeaderDiscovered,
}

pub struct RaftRole {
    fsm: FiniteStateMachine<NodeRole, NodeEvent>,
    current_state: NodeRole,
    term: u64,
    quorum: u16,
    node_id: RaftNodeId,
    voted: RaftNodeId,
    leader_id: RaftNodeId,

    election_tick_min: usize,
    election_tick_max: usize,
    election_timeout_randomized: usize,
    election_elapsed: usize,
    heartbeat_tick: usize,
    heartbeat_elapsed: usize,
}

impl RaftRole {
    pub fn new(config: &RaftConfig) -> Self {
        let mut new_fsm =
            FiniteStateMachine::<NodeRole, NodeEvent>::new("ConsensusRaft".to_owned());
        new_fsm.add_state("Follower", &NodeRole::Follower);
        new_fsm.add_state("Candidate", &NodeRole::Candidate);
        new_fsm.add_state("Leader", &NodeRole::Leader);

        new_fsm.add_event("HeartBeatReceived", &NodeEvent::OnAppendEntries);
        new_fsm.add_event("ElectionTimeout", &NodeEvent::OnElectionTimeout);
        new_fsm.add_event("VoteRequested", &NodeEvent::OnVoteRequest);
        new_fsm.add_event("VoteReceived", &NodeEvent::OnVoteReceived);
        new_fsm.add_event("VoteMajorityReceived", &NodeEvent::OnVoteMajorityReceived);
        new_fsm.add_event("LeaderFound", &NodeEvent::OnLeaderDiscovered);

        new_fsm.add_transition("Follower", "HeartBeatReceived", "Follower");
        new_fsm.add_transition("Follower", "VoteRequested", "Follower");
        new_fsm.add_transition("Follower", "LeaderFound", "Follower");
        new_fsm.add_transition("Follower", "ElectionTimeout", "Candidate");
        new_fsm.add_transition("Candidate", "HeartBeatReceived", "Follower");
        new_fsm.add_transition("Candidate", "LeaderFound", "Follower");
        new_fsm.add_transition("Candidate", "ElectionTimeout", "Candidate");
        new_fsm.add_transition("Candidate", "VoteReceived", "Candidate");
        new_fsm.add_transition("Candidate", "VoteMajorityReceived", "Leader");
        new_fsm.add_transition("Leader", "HeartBeatReceived", "Follower");
        new_fsm.add_transition("Leader", "LeaderFound", "Follower");

        Self {
            fsm: new_fsm,
            current_state: NodeRole::default(),
            term: 0,
            quorum: 0,
            node_id: config.node_id,
            voted: RAFT_INVALID_NODE_ID,
            leader_id: RAFT_INVALID_NODE_ID,
            election_tick_min: config.min_election_tick(),
            election_tick_max: config.max_election_tick(),
            election_timeout_randomized: 0,
            election_elapsed: 0,
            heartbeat_tick: config.heartbeat_tick,
            heartbeat_elapsed: 0,
        }
    }

    fn next_state(&self, event: &NodeEvent) -> FsmState<NodeRole> {
        self.fsm.next_state(&self.current_state, event)
    }

    pub fn randomized_election_tick(&mut self) -> usize {
        let mut rng = rand::thread_rng();
        let tick_range = Uniform::from(self.election_tick_min..=self.election_tick_max);
        self.election_timeout_randomized = tick_range.sample(&mut rng);
        info!(
            "node #{}: Election timeout randomized: {} [{}-{}]",
            self.node_id, self.election_timeout_randomized, self.election_tick_min, self.election_tick_max
        );
        self.election_timeout_randomized
    }

    fn tick_election(&mut self) -> bool {
        if self.election_timeout_randomized == 0 {
            self.randomized_election_tick();
        }

        debug!(
            "node #{}: election: current_state={:?} tick_required={} elapsed={}",
            self.node_id, self.current_state, self.election_timeout_randomized, self.election_elapsed
        );

        self.election_elapsed += 1;
        if self.election_elapsed < self.election_timeout_randomized {
            return false;
        }

        self.election_elapsed = 0;
        self.randomized_election_tick();
        match self.next_state(&NodeEvent::OnElectionTimeout) {
            FsmState::State(new_role) => {
                if new_role == NodeRole::Candidate {
                    self.current_state = new_role;
                    self.campaign();
                    true
                } else {
                    todo!("handle other scenario");
                }
            }
            FsmState::InvalidState => false,
        }
    }

    fn tick_heartbeat(&mut self) -> bool {
        self.heartbeat_elapsed += 1;
        if self.heartbeat_elapsed < self.heartbeat_tick {
            return false;
        }

        self.heartbeat_elapsed = 0;
        self.append_entries();
        true
    }

    fn broadcast_to_peers(&self, msg: RaftMessageType) {
        info!("node #{}: Broadcasting {:?} to peers", self.node_id, msg);
    }

    fn campaign(&mut self) {
        info!("node #{}: start campaigning", self.node_id);
        self.term += 1;
        self.quorum = 1;
        self.voted = self.node_id;

        let m = RaftMessageType::MsgRequestVote;
        self.broadcast_to_peers(m);
    }

    fn append_entries(&self) {
        info!("node #{}: start append entries", self.node_id);
        let m = RaftMessageType::MsgAppendEntries;
        self.broadcast_to_peers(m);
    }

    pub fn node_id(&self) -> RaftNodeId {
        self.node_id
    }

    pub fn voted(&self) -> RaftNodeId {
        self.voted
    }

    pub fn leader_id(&self) -> RaftNodeId {
        self.leader_id
    }

    pub fn current_state(&self) -> &NodeRole {
        &self.current_state
    }

    pub fn current_term(&self) -> u64 {
        self.term
    }

    pub fn election_tick(&self) -> usize {
        self.election_timeout_randomized
    }

    pub fn election_elapsed(&self) -> usize {
        self.election_elapsed
    }

    pub fn min_election_tick(&self) -> usize {
        self.election_tick_min
    }

    pub fn max_election_tick(&self) -> usize {
        self.election_tick_max
    }

    pub fn heartbeat_tick(&self) -> usize {
        self.heartbeat_tick
    }

    pub fn heartbeat_elapsed(&self) -> usize {
        self.heartbeat_elapsed
    }

    pub fn tick(&mut self) {
        match self.current_state {
            NodeRole::Follower | NodeRole::Candidate => {
                self.tick_election();
            }
            NodeRole::Leader => {
                self.tick_heartbeat();
                self.tick_election();
            }
        }
    }

    pub fn on_request_vote(&self) {}

    pub fn on_request_append_entries(&self) {}

    pub fn request_vote() {}
}
