mod tests {
    use super::super::*;
    use crate::cluster::*;
    use crate::message::*;
    use log::{debug, info, LevelFilter};
    use simple_logger::SimpleLogger;
    use futures::{{select}, future::{select_all, FutureExt}, executor::block_on};
    use network::{Network, Config};
    use std::sync::Once;
    use std::{thread, time::{Duration, Instant}};
    use std::collections::HashMap;

    static INIT: Once = Once::new();

    fn initialize() {
        INIT.call_once(|| {
            SimpleLogger::new()
                .with_level(LevelFilter::Debug)
                .init()
                .expect("unable to start logger for raft unit test");
        });
    }

    async fn raft_time_limit(duration_ms: u64) {
        let started = Instant::now();
        let timeout = Duration::from_millis(duration_ms);
        info!("task started: {:?}", started);
        loop {
            thread::sleep(Duration::from_millis(1));
            if started.elapsed() >= timeout {
                break;
            }
        }
        info!("task ended after: {:?}", started.elapsed());
    }

    fn raft_config_default(node_id: Option<RaftNodeId>) -> RaftConfig {
        match node_id {
            Some(id) => {
                let mut config = RaftConfig::default();
                config.node_id = id;
                config
            }
            None => RaftConfig::default(),
        }
    }

    fn new_raft_cluster_single(config: &RaftConfig) -> RaftCluster {
        RaftCluster::new(config, &mut None)
    }

    fn new_raft_cluster_five_nodes() -> RaftCluster {
        let mut cluster = RaftCluster::new(&raft_config_default(Some(1)), &mut None);
        assert!(cluster.join_cluster(2));
        assert!(cluster.join_cluster(3));
        assert!(cluster.join_cluster(4));
        assert!(cluster.join_cluster(5));
        cluster
    }

    fn new_raft_network_cluster(node_id: RaftNodeId) -> RaftCluster {
        let conf = Config::default();
        let net = Network::new(conf, node_id).unwrap();
        RaftCluster::new(&raft_config_default(Some(node_id)), &mut Some(net))
    }

    fn new_raft_multi_cluster() -> HashMap<RaftNodeId, RaftCluster> {
        let mut clusters = HashMap::<RaftNodeId, RaftCluster>::new();
        let mut cluster1 = RaftCluster::new(&raft_config_default(Some(1)), &mut None);
        let mut cluster2 = RaftCluster::new(&raft_config_default(Some(2)), &mut None);

        cluster1.join_cluster(cluster2.node_id());
        cluster2.join_cluster(cluster1.node_id());

        clusters.insert(cluster1.node_id(), cluster1);
        clusters.insert(cluster2.node_id(), cluster2);
        clusters
    }

    fn new_raft_multi_cluster_with_network(cluster_count: u8) -> HashMap<RaftNodeId, RaftCluster> {
        let mut clusters = HashMap::<RaftNodeId, RaftCluster>::new();

        for node_id in 1..=cluster_count {
            let cluster = new_raft_network_cluster(node_id as RaftNodeId);
            clusters.insert(cluster.node_id(), cluster);
        }
        clusters
    }

    #[test]
    fn raft_node_new_auto_node_id() {
        let cluster = new_raft_cluster_single(&raft_config_default(Some(0)));
        assert_ne!(cluster.node_id(), 0);
    }

    #[test]
    fn raft_node_new_valid() {
        let cluster = new_raft_cluster_single(&raft_config_default(Some(1)));
        assert_eq!(cluster.node_id(), 1);
        assert_eq!(cluster.node().node_role(), NodeRole::Follower);
        assert_eq!(cluster.node().current_leader(), RAFT_INVALID_NODE_ID);
    }

    #[test]
    fn raft_node_tick_range() {
        initialize();
        let mut cluster = new_raft_cluster_single(&raft_config_default(None));
        {
            let node = cluster.node_mut();
            for _ in 0..100 {
                let min_timeout = node.min_timeout();
                let max_timeout = node.max_timeout();
                let random_timeout = node.role.randomized_election_tick();
                assert!(min_timeout < max_timeout);
                assert!(random_timeout >= min_timeout && random_timeout <= max_timeout);
                assert_eq!(node.election_timeout(), random_timeout);
            }
        }
    }

    #[test]
    fn raft_multi_cluster_initialize() {
        let clusters = new_raft_multi_cluster();
        let node1= clusters.get(&1).unwrap();
        let node2 = clusters.get(&2).unwrap();

        assert_eq!(node1.size(), 2);
        assert_eq!(node2.size(), 2);
    }

    #[test]
    #[should_panic]
    fn raft_multi_cluster_start_without_network() {
        let mut clusters = new_raft_multi_cluster();
        let node1 = clusters.get_mut(&1).unwrap();
        block_on(node1.start());
    }

    #[test]
    fn raft_multi_cluster_start_network() {
        initialize();
        let cluster_count = 3;
        let test_time = 5000;
        let mut clusters = new_raft_multi_cluster_with_network(cluster_count);
        let nodes_count = clusters.len();
        let mut all_active_nodes : Vec<_> = clusters.values_mut().into_iter().map(|node| Box::pin(node.start())).collect();

        info!("number of cluster:{}, active:{}", nodes_count, all_active_nodes.len());
        assert_eq!(nodes_count, cluster_count as usize);
        assert_eq!(all_active_nodes.len(), cluster_count as usize);

        let run_all_nodes = async {
            while ! all_active_nodes.is_empty() {
                match select_all(all_active_nodes).await {
                    ((), index, remaining) => {
                        info!("task idx {} finished [remaining: {}]", index, remaining.len());
                        all_active_nodes = remaining;
                    },
                }
            }
        }.fuse();

        block_on(async {
            info!("starting async test block");
            //loop {
                select! {
                    _ = Box::pin(run_all_nodes) => {
                        info!("completed: all nodes completed");
                        //break;
                    },

                    () = raft_time_limit(test_time).fuse() => {
                        info!("completed: terminating test after {}ms test time", test_time);
                        //break;
                    },

                    complete => {
                        info!("completed?");
                        //break
                    },
                }
            //}
        });
    }

    #[test]
    fn raft_tick_election() {
        initialize();
        info!("starting raft tick election");
        let handle = thread::spawn(move || {
            let mut cluster = new_raft_cluster_single(&raft_config_default(None));
            let node = cluster.node_mut();
            let mut expected_role = NodeRole::Follower;

            let sleep_interval = 10;
            let mut limit = 150;
            loop {
                debug!("node #{}: sleeping for {}ms", node.node_id(), sleep_interval);
                thread::sleep(Duration::from_millis(sleep_interval));
                limit -= 1;
                if limit <= 0 {
                    break;
                };
                let current_election_elapsed = node.role().election_elapsed();
                assert_eq!(node.node_role(), expected_role);
                node.tick();
                if expected_role == NodeRole::Follower && node.role().election_elapsed() == 0 {
                    expected_role = NodeRole::Candidate;
                }
                assert!(
                    node.role().election_elapsed() == current_election_elapsed + 1
                        || node.role().election_elapsed() == 0
                );
            }
        });
        handle.join().unwrap();
    }

    #[test]
    fn raft_tick_heartbeat() {
        initialize();
        info!("starting raft tick heartbeat");
        let handle = thread::spawn(move || {
            let mut cluster = new_raft_cluster_single(&raft_config_default(None));
            let node = cluster.node_mut();

            let sleep_interval = 10;
            let mut limit = 150;
            loop {
                debug!("node #{}: sleeping for {}ms", node.node_id(), sleep_interval);
                thread::sleep(Duration::from_millis(sleep_interval));
                limit -= 1;
                if limit <= 0 {
                    break;
                };
                let current_heartbeat_elapsed = node.role().heartbeat_elapsed();
                node.tick();
                assert!(
                    node.role().heartbeat_elapsed() == current_heartbeat_elapsed + 1
                        || node.role().heartbeat_elapsed() == 0
                );
                // todo!("need to test leader sending entries");
            }
        });
        handle.join().unwrap();
    }

    #[test]
    fn raft_cluster_peer_join() {
        initialize();
        let cluster = new_raft_cluster_five_nodes();
        assert_eq!(cluster.size(), 5);
        assert_eq!(cluster.quorum_needed(), 3);
    }

    #[test]
    fn raft_cluster_peer_leave() {
        initialize();
        let mut cluster = new_raft_cluster_five_nodes();
        assert!(cluster.leave_cluster(2));
        assert!(cluster.leave_cluster(4));
        assert_eq!(cluster.size(), 3);
        assert_eq!(cluster.quorum_needed(), 2);
    }

    #[test]
    fn raft_message_type() {
        assert_eq!(RaftMessageType::MsgAppendEntries.ordinal(), 0);
        assert_eq!(RaftMessageType::MsgAppendEntriesResponse.ordinal(), 1);
        assert_eq!(RaftMessageType::MsgRequestVote.ordinal(), 2);
        assert_eq!(RaftMessageType::MsgRequestVoteResponse.ordinal(), 3);
        assert_eq!(RaftMessageType::MsgTransferLeader.ordinal(), 4);

        assert_eq!(
            Some(RaftMessageType::MsgAppendEntries),
            RaftMessageType::from_ordinal(0)
        );
        assert_eq!(
            Some(RaftMessageType::MsgAppendEntriesResponse),
            RaftMessageType::from_ordinal(1)
        );
        assert_eq!(
            Some(RaftMessageType::MsgRequestVote),
            RaftMessageType::from_ordinal(2)
        );
        assert_eq!(
            Some(RaftMessageType::MsgRequestVoteResponse),
            RaftMessageType::from_ordinal(3)
        );
        assert_eq!(
            Some(RaftMessageType::MsgTransferLeader),
            RaftMessageType::from_ordinal(4)
        );
    }

    #[test]
    fn raft_message_entry_type() {
        assert_eq!(RaftMsgEntryType::MsgEntryNormal.ordinal(), 0);
        assert_eq!(RaftMsgEntryType::MsgEntryConfigChange.ordinal(), 1);

        assert_eq!(
            Some(RaftMsgEntryType::MsgEntryNormal),
            RaftMsgEntryType::from_ordinal(0)
        );
        assert_eq!(
            Some(RaftMsgEntryType::MsgEntryConfigChange),
            RaftMsgEntryType::from_ordinal(1)
        );
    }

    #[test]
    fn raft_message_creation() {
        let mut m = RaftMessage::new(RaftMessageType::MsgRequestVote, 1, 10);
        assert_eq!(m.msg_type(), RaftMessageType::MsgRequestVote);
        assert_eq!(m.is_from(), RAFT_INVALID_NODE_ID);
        assert_eq!(m.is_to(), RAFT_INVALID_NODE_ID);
        assert_eq!(m.term(), 1);
        assert_eq!(m.index(), 10);

        let m = m.from(100).clone_to(999);
        assert_eq!(m.is_from(), 100);
        assert_eq!(m.is_to(), 999);
    }

    #[test]
    fn raft_message_serde() {
        let m = RaftMessage::new(RaftMessageType::MsgRequestVote, 5, 99);
        assert_eq!(m.msg_type(), RaftMessageType::MsgRequestVote);
        let msg_bytes = serde_cbor::ser::to_vec(&m).unwrap();
        println!("msg={:?}", msg_bytes);
        let msg: RaftMessage = serde_cbor::de::from_slice(&msg_bytes[..]).unwrap();
        assert_eq!(msg.is_from(), m.is_from());
        assert_eq!(msg.is_to(), m.is_to());
        assert_eq!(msg.msg_type(), m.msg_type());
        assert_eq!(msg.term(), m.term());
        assert_eq!(msg.index(), m.index());
        assert_eq!(msg.payload_length(), m.payload_length());
    }

    #[test]
    #[ignore]
    fn raft_message_broadcast() {
        initialize();
        let _node = new_raft_cluster_five_nodes();
        todo!();
        // futures::executor::block_on(node.broadcast(RaftMessage::new(
        //     RaftMessageType::MsgAppendEntries,
        //     1,
        //     1,
        // )));
    }

    #[test]
    fn raft_cluster() {
        initialize();
        let cluster = new_raft_cluster_single(&raft_config_default(None));
        let node = cluster.node();
        assert_eq!(cluster.size(), 1);
        assert_eq!(cluster.quorum_needed(), 1);
        assert_eq!(cluster.node_id(), node.node_id());
    }

    #[test]
    #[ignore]
    fn raft_just_heartbeat() {
        todo!()
    }

    #[test]
    #[ignore]
    fn raft_append_entries() {
        todo!()
    }

    #[test]
    #[ignore]
    fn raft_become_candidate() {
        todo!()
    }

    #[test]
    #[ignore]
    fn raft_become_leader() {
        todo!()
    }

    #[test]
    #[ignore]
    fn raft_become_follower() {}

    #[test]
    #[ignore]
    fn raft_vote_request() {
        todo!()
    }

    #[test]
    #[ignore]
    fn raft_vote_accept() {
        todo!()
    }

    #[test]
    #[ignore]
    fn raft_vote_reject() {
        todo!()
    }

    #[test]
    #[ignore]
    fn raft_run_server() {
        todo!()
    }
}
