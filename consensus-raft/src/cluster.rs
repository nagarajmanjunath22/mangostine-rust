use crate::message::RaftMessage;
use crate::raft::RaftNode;
use crate::raft::RaftNodeId;
use crate::config::RaftConfig;
use std::collections::HashSet;
use std::{thread, time::Duration};
use std::sync::mpsc;
use futures::executor::block_on;
use async_std::sync::{Receiver, Sender};
use network::{MessageEntry, Network};
use log::{debug, info, error};

pub struct RaftNetwork {
    _topic_name: String,
    _sender: Sender<MessageEntry>,
    receiver: Receiver<MessageEntry>,
}

impl RaftNetwork {
    pub fn new(network: &mut Network) -> Self {
        let _topic_name = format!("/mangostine/{}/consensus", network.name());
        let _sender = network.sender();
        let receiver = network.register_topic(_topic_name.clone());
        Self {
            _topic_name,
            _sender,
            receiver,
        }
    }
}

pub struct RaftCluster {
    node: RaftNode,
    network: Option<RaftNetwork>,
    peers: HashSet<RaftNodeId>,
}

impl RaftCluster {
    pub fn new(config: &RaftConfig, network: &mut Option<Network>) -> Self {
        let node = RaftNode::new(config);

        let raft_network: Option<RaftNetwork> = match network {
            Some(net) => Some(RaftNetwork::new(net)),
            None => None,
        };

        Self {
            node: node,
            network: raft_network,
            peers: HashSet::new(),
        }
    }

    pub async fn start(&mut self) {
        if let Some(raft_network) = &self.network {
            info!("starting raft cluster for node: {}", self.node_id());
            let tick_duration_ms = self.node.tick_duration_ms();
            let node_id = self.node_id();

            let (tx, rx) = mpsc::channel();
            debug!("spawning ticker for node: {}", node_id);
            thread::spawn(move || {
                loop {
                    if let Err(err) = tx.send(true) {
                        panic!("unable to send signal from node {} to cluster, err:{}", node_id, err);
                    }
                    debug!("node #{}: sleeping for {}ms", node_id, tick_duration_ms);
                    thread::sleep(Duration::from_millis(tick_duration_ms));
                }
            });

            let network_receiver = raft_network.receiver.clone();
            debug!("spawning network receiver for node: {}", node_id);
            thread::spawn(move || {
                loop {
                    block_on(
                        async {
                            if let Ok(_net_msg) = network_receiver.recv().await {
                            }
                        }
                    );
                    thread::sleep(Duration::from_millis(1));
                }
            });

            loop {
                if let Ok(node_tick_timeout) = rx.try_recv() {
                    if node_tick_timeout {
                        self.node.tick();
                    }
                }

                if let Ok(in_msg) = raft_network.receiver.try_recv() {
                    match in_msg.to {
                        Some(recipient) => {
                            if recipient == node_id {
                                self.dispatch(&in_msg);
                            }
                        }
                        None => {
                            self.dispatch(&in_msg);
                        }
                    }
                }
                thread::sleep(Duration::from_millis(1));
            }
        } else {
            panic!("failed to start cluster, network module dependency is required");
        }
    }

    pub fn dispatch(&self, in_msg: &MessageEntry) {
        let node_id = self.node_id();
        let mut msg = None;
        match in_msg.to {
            Some(recipient) => {
                if recipient == node_id {
                    msg = Some(&in_msg.message);
                }
            }
            None => {
                msg = Some(&in_msg.message);
            }
        };

        if let Some(payload) = msg {
            if let Ok(raft_msg) = serde_cbor::de::from_slice::<RaftMessage>(&payload[..]) {
                self.node.on_msg(raft_msg);
            } else {
                error!("ignoring unknown data");
            }
        }
    }

    pub fn node<'rc>(&'rc self) -> &'rc RaftNode {
        &self.node
    }

    pub fn node_mut<'rc>(&'rc mut self) -> &'rc mut RaftNode {
        &mut self.node
    }

    pub fn node_id(&self) -> RaftNodeId {
        self.node.node_id()
    }

    pub fn size(&self) -> usize {
        self.peers.len() + 1
    }

    pub fn quorum_needed(&self) -> usize {
        (self.size() / 2) + 1
    }

    pub fn join_cluster(&mut self, peer: RaftNodeId) -> bool {
        if peer == self.node_id() {
            error!("cannot add peers with same node id as self");
            return false;
        }

        if self.peers.contains(&peer) {
            error!("cannot add duplicate peer");
            return false;
        }

        self.peers.insert(peer);
        true
    }

    pub fn leave_cluster(&mut self, peer_node_id: RaftNodeId) -> bool {
        if self.peers.contains(&peer_node_id) {
            self.peers.remove(&peer_node_id);
            return true;
        }
        error!(
            "cannot leave cluster as peer node {} not found",
            peer_node_id
        );
        false
    }

    // pub async fn broadcast_msg(&self, msg: &mut RaftMessage) {
    //     msg.from(self.node_id());
    //     for peer in self.peers {
    //         let m = msg.clone_to(peer);
    //         info!("broadcasting {:?} to {}", m, peer);
    //         peer.in_message(m).await;
    //     }
    // }


}
