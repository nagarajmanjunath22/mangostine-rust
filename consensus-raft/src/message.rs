use crate::enum_ordinalize::*;
use crate::raft::RaftNodeId;
use crate::raft::RAFT_INVALID_NODE_ID;
use crate::serde_derive::*;
use std::vec::Vec;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, Ordinalize)]
pub enum RaftMessageType {
    MsgAppendEntries,
    MsgAppendEntriesResponse,
    MsgRequestVote,
    MsgRequestVoteResponse,
    MsgTransferLeader,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RaftMessageHeader {
    pub message_type: RaftMessageType,
    pub from: RaftNodeId,
    pub to: RaftNodeId,
    pub term: usize,
    pub index: usize,
}

impl RaftMessageHeader {
    pub fn new(message_type: RaftMessageType, term: usize, index: usize) -> Self {
        Self {
            message_type: message_type,
            from: RAFT_INVALID_NODE_ID,
            to: RAFT_INVALID_NODE_ID,
            term,
            index,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RaftMessage {
    header: RaftMessageHeader,
    payload: Vec<u8>,
}

impl RaftMessage {
    pub fn new(message_type: RaftMessageType, term: usize, index: usize) -> Self {
        Self {
            header: RaftMessageHeader::new(message_type, term, index),
            payload: Vec::new(),
        }
    }

    pub fn from(&mut self, from: RaftNodeId) -> &Self {
        self.header.from = from;
        self
    }

    pub fn to(&mut self, to: RaftNodeId) -> &Self {
        self.header.to = to;
        self
    }

    pub fn clone_to(&self, to: RaftNodeId) -> Self {
        let mut to_msg = self.clone();
        to_msg.header.to = to;
        to_msg
    }

    pub fn is_from(&self) -> RaftNodeId {
        self.header.from
    }

    pub fn is_to(&self) -> RaftNodeId {
        self.header.to
    }

    pub fn term(&self) -> usize {
        self.header.term
    }

    pub fn index(&self) -> usize {
        self.header.index
    }

    pub fn msg_type(&self) -> RaftMessageType {
        self.header.message_type.clone()
    }

    pub fn payload_length(&self) -> usize {
        self.payload.len()
    }

    pub fn payload(&self) -> &Vec<u8> {
        &self.payload
    }
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize, Ordinalize)]
pub enum RaftMsgEntryType {
    MsgEntryNormal,
    MsgEntryConfigChange,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RaftMsgEntry {
    entry_type: RaftMessageType,
    term: usize,
    index: usize,
    msg_length: usize,
    msg: Vec<u8>,
}

impl RaftMsgEntry {
    pub fn new(entry_type: RaftMessageType, term: usize, index: usize) -> Self {
        Self {
            entry_type,
            term,
            index,
            msg_length: 0,
            msg: Vec::new(),
        }
    }
}
