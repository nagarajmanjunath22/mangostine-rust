use crate::errors::RaftError;
use crate::raft::RaftNodeId;
use crate::raft::RAFT_INVALID_NODE_ID;
use crate::serde_derive::*;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct RaftConfig {
    pub node_id: RaftNodeId,
    pub election_tick: usize,
    pub heartbeat_tick: usize,
    pub min_election_tick: usize,
    pub max_election_tick: usize,
    pub election_priority: usize,
    pub tick_duration_ms: u64,
}

impl Default for RaftConfig {
    fn default() -> Self {
        const HEARTBEAT_TICK: usize = 2;
        Self {
            node_id: RAFT_INVALID_NODE_ID,
            election_tick: HEARTBEAT_TICK * 10,
            heartbeat_tick: HEARTBEAT_TICK,
            min_election_tick: 0,
            max_election_tick: 0,
            election_priority: 0,
            tick_duration_ms: 10,
        }
    }
}

impl RaftConfig {
    pub fn new(node_id: RaftNodeId) -> Self {
        Self {
            node_id,
            ..Self::default()
        }
    }

    pub fn min_election_tick(&self) -> usize {
        if self.min_election_tick == 0 {
            self.election_tick
        } else {
            self.min_election_tick
        }
    }

    pub fn max_election_tick(&self) -> usize {
        if self.max_election_tick == 0 {
            self.election_tick * 2
        } else {
            self.max_election_tick
        }
    }

    pub fn validate(&self) -> Result<(), RaftError> {
        if self.heartbeat_tick == 0 {
            return Err(RaftError::ConfigInvalid(
                "heartbeat tick must be greater than 0".to_owned(),
            ));
        }

        if self.election_tick <= self.heartbeat_tick {
            return Err(RaftError::ConfigInvalid(
                "election tick must be greater than heartbeat tick".to_owned(),
            ));
        }

        let min_timeout = self.min_election_tick();
        let max_timeout = self.max_election_tick();

        if min_timeout >= max_timeout {
            return Err(RaftError::ConfigInvalid(format!(
                "min election tick {} should be less than max election tick {}",
                min_timeout, max_timeout
            )));
        }

        if self.tick_duration_ms <= 0 {
            return Err(RaftError::ConfigInvalid(
                "tick duration (ms) must be greater than 0".to_owned(),
            ));
        }

        Ok(())
    }
}
