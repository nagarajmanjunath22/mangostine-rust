use std::error::Error;
use std::fmt;

#[derive(PartialEq, Eq, Clone, Debug)]
pub enum MgError {
    NotExists,
    Exists,
    Duplicate(String),
    Mismatched(String),
    IOError(i32, String),
    BlockRequestError(String),
    AccountRequestError(String),
    InvalidTx(String),
    InvalidHeader(String),
    InvalidAccount(String),
    InvalidBlock(String),
    BroadcastMsgFailed,
    InvalidSignatureLength,
    InvalidPubKeyLength,
    ParsingError,
    InvalidAddress,
    Other(String),
}

pub type MgResult<T> = Result<T, MgError>;

impl Error for MgError {}

impl fmt::Display for MgError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
        // or, alternatively:
        // fmt::Debug::fmt(self, f)
    }
}
