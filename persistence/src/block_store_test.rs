mod tests {
    use super::super::*;
    use chrono::Utc;
    use primitive_types::{H256, U256};
    use std::default::Default;
    use types::address;
    use types::block::header::{Header, Version};
    use types::block::Block;
    use types::transaction;

    #[test]
    fn block_store_test() {
        let block_store: BlockStore = BlockStore::open("test-rocksdb");
        let chain_id = "mangostine".to_owned();
        let block_number = U256::from_dec_str("0").unwrap();
        let block_header = Header::new(
            block_number,
            H256::random(),
            H256::random(),
            Utc::now(),
            H256::random(),
            chain_id,
            Version::default(),
        );

        let mut transactions: Vec<transaction::Transaction> = Vec::new();

        let transaction_1 = transaction::Transaction::new(
            address::Address::new(Default::default()),
            address::Address::new(Default::default()),
            u64::MAX,
            0
        );
        transactions.push(transaction_1);

        let block_0 = Block::new(block_header, transactions);
        println!("block {:?}", &block_0);

        // execute block for the first time should be ok.
        assert_eq!(block_store.execute_block(&block_0).is_ok(), true);

        // execute block for the 2nd time should not ok(same blockhash).
        assert_eq!(block_store.execute_block(&block_0).is_ok(), false);

        // retrieve block with block number, make sure test_block = block_0.
        let test_block = block_store.get_block(Some(block_number), None).unwrap();
        assert_eq!(test_block, block_0);

        // retrieve block with block hash, make sure test_block_1 = block_0
        let test_block_1 = block_store
            .get_block(None, Some(block_0.block_hash()))
            .unwrap();
        assert_eq!(test_block_1, block_0);

        // retrieve block header with block number, make sure test_block_header = block_0.header.
        let test_block_header = block_store
            .get_block_header(Some(block_number), None)
            .unwrap();
        assert_eq!(test_block_header, block_0.header);

        // retrieve block header with block hash, make sure test_block_header_1 = block_0.header.
        let test_block_header_1 = block_store
            .get_block_header(None, Some(block_0.block_hash()))
            .unwrap();
        assert_eq!(test_block_header_1, block_0.header);

        // retrieve transaction with tx hash, make sure tx = block_0.transactions[0].
        let tx = block_store
            .get_transaction(block_0.transactions[0].hash())
            .unwrap();
        assert_eq!(tx, block_0.transactions[0]);
        println!("{:?}", tx);

        // finish test must drop and destroy.
        drop(block_store);
        KVStore::destroy("test-rocksdb/data/mangostine_block_store");
        KVStore::destroy("test-rocksdb/data");
        KVStore::destroy("test-rocksdb");
    }
}
