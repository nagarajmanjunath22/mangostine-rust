mod tests {
    use super::super::*;
    use chrono::Utc;
    use primitive_types::{H256, U256};
    use std::default::Default;
    use types::address;
    use types::block::header::{Header, Version};
    use types::block::Block;
    use types::transaction;

    #[test]
    fn store_test() {
        let block_number = U256::from_dec_str("0").unwrap();
        let chain_id = "mangostine".to_owned();
        let block_header = Header::new(
            block_number,
            H256::random(),
            H256::random(),
            Utc::now(),
            H256::zero(),
            chain_id,
            Version::default(),
        );

        let mut transactions: Vec<transaction::Transaction> = Vec::new();
        let transaction_1 = transaction::Transaction::new(
            address::Address::new(Default::default()),
            address::Address::new(Default::default()),
            u64::MAX,
            0
        );
        transactions.push(transaction_1);

        let block_0 = Block::new(block_header, transactions);
        let db_store: KVStore = Database::init("test-rocksdb");
        let key = block_0.block_hash();
        let key_bz: &[u8] = &key.as_bytes();

        let block_bz = serde_cbor::ser::to_vec(&block_0).unwrap();
        db_store.set(&key_bz, &block_bz);
        assert_eq!(db_store.has(key_bz), true);

        assert_eq!(db_store.get(key_bz).unwrap(), block_bz);

        db_store.delete(key_bz);
        assert_eq!(db_store.has(key_bz), false);

        // finish test must drop and destroy.
        drop(db_store);
        KVStore::destroy("test-rocksdb");
    }

    #[test]
    fn store_test_column_family() {
        let mut cols: Vec<&str> = Vec::new();
        cols.push("column1");
        cols.push("column2");
        let col1 = cols[0];
        let col2 = cols[1];

        // open a db with two column.
        let db_store: KVStore = Database::open_cf("test_rocksdb_cf", cols);

        // test column 1.
        let key = String::from("testkey1").as_bytes().to_owned();
        let val = String::from("test1").as_bytes().to_owned();
        db_store.set_cf(col1, &key, &val);
        assert_eq!(db_store.has_cf(col1, &key), true);
        assert_eq!(db_store.get_cf(col1, &key).unwrap(), val);

        // test column 2.
        let key2 = String::from("testkey2").as_bytes().to_owned();
        let val2 = String::from("test2").as_bytes().to_owned();
        db_store.set_cf(col2, &key2, &val2);
        assert_eq!(db_store.has_cf(col2, &key2), true);
        assert_eq!(db_store.get_cf(col2, &key2).unwrap(), val2);

        // finish test must drop and destroy.
        drop(db_store);
        KVStore::destroy("test_rocksdb_cf");
    }
}
