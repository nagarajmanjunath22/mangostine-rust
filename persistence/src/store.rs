use rocksdb::{ColumnFamilyDescriptor, IteratorMode, Options, DB};
use std::sync::{Arc, Mutex};

pub trait Database {
    fn init(file_path: &str) -> Self;
    fn set(&self, k: &[u8], v: &[u8]) -> bool;
    fn get(&self, k: &[u8]) -> Option<Vec<u8>>;
    fn delete(&self, k: &[u8]) -> bool;
    fn has(&self, k: &[u8]) -> bool;
    fn get_all(&self) -> Option<Vec<(Box<[u8]>, Box<[u8]>)>>;
    // use with column family.
    fn open_cf(file_path: &str, col: Vec<&str>) -> Self;
    fn set_cf(&self, col: &str, key: &[u8], val: &[u8]) -> bool;
    fn get_cf(&self, col: &str, key: &[u8]) -> Option<Vec<u8>>;
    fn has_cf(&self, col: &str, key: &[u8]) -> bool;

    fn destroy(file_path: &str);
}

#[derive(Clone)]
pub struct KVStore {
    db: Arc<Mutex<DB>>,
}

impl Database for KVStore {
    fn init(file_path: &str) -> Self {
        KVStore {
            db: Arc::new(Mutex::new(DB::open_default(file_path).unwrap())),
        }
    }

    fn get(&self, k: &[u8]) -> Option<Vec<u8>> {
        match self.db.lock().unwrap().get(k) {
            Ok(Some(v)) => Some(v),
            Ok(None) => None,
            Err(_e) => None,
        }
    }

    fn get_all(&self) -> Option<Vec<(Box<[u8]>, Box<[u8]>)>> {
        let db_value = self.db.lock().unwrap();
        let iter = db_value.iterator(IteratorMode::Start); // Always iterates forward
        let mut kv = Vec::new();
        for (key, value) in iter {
            kv.push((key, value))
        }
        return Some(kv);
    }

    fn has(&self, k: &[u8]) -> bool {
        match self.db.lock().unwrap().get(k) {
            Ok(Some(_val)) => true,
            Ok(None) => false,
            Err(_e) => false,
        }
    }

    fn set(&self, k: &[u8], v: &[u8]) -> bool {
        self.db.lock().unwrap().put(k, v).is_ok()
    }

    fn delete(&self, k: &[u8]) -> bool {
        self.db.lock().unwrap().delete(k).is_ok()
    }

    fn destroy(file_path: &str) {
        let db_opts = Options::default();
        DB::destroy(&db_opts, file_path).unwrap()
    }

    // store with column family.
    fn open_cf(file_path: &str, cols: Vec<&str>) -> Self {
        let mut cfs: Vec<ColumnFamilyDescriptor> = Vec::new();

        for col in cols {
            let cf_opts = Options::default();
            cfs.push(ColumnFamilyDescriptor::new(col, cf_opts));
        }

        let mut db_opts = Options::default();
        db_opts.create_missing_column_families(true);
        db_opts.create_if_missing(true);
        KVStore {
            db: Arc::new(Mutex::new(
                DB::open_cf_descriptors(&db_opts, file_path, cfs).unwrap(),
            )),
        }
    }

    fn set_cf(&self, col: &str, key: &[u8], val: &[u8]) -> bool {
        let db = self.db.lock().unwrap();
        let cf = db.cf_handle(col).unwrap();
        db.put_cf(cf, key, val).is_ok()
    }

    fn get_cf(&self, col: &str, key: &[u8]) -> Option<Vec<u8>> {
        let db = self.db.lock().unwrap();
        let cf = db.cf_handle(col).unwrap();
        match db.get_cf(cf, key) {
            Ok(Some(v)) => Some(v),
            Ok(None) => None,
            Err(_e) => None,
        }
    }

    fn has_cf(&self, col: &str, key: &[u8]) -> bool {
        let db = self.db.lock().unwrap();
        let cf = db.cf_handle(col).unwrap();
        match db.get_cf(cf, key) {
            Ok(Some(_v)) => true,
            Ok(None) => false,
            Err(_e) => false,
        }
    }
}

// impl KVStore {
//     fn lock(&self) -> DB {}
// }
#[cfg(test)]
#[path = "./store_test.rs"]
mod store_test;
