use crate::store::{self, Database, KVStore};
use error::error::MgError;
use log::error;
use std::collections::HashMap;
use std::convert::TryInto;
use types::account::Account;
use types::address::Address;

pub const ACCOUNTSTORE_DIR: &str = "/data/mangostine_account_store";
pub struct AccountStore {
    db: KVStore,
}

impl AccountStore {
    // init account store.
    pub fn open(path: &str) -> Self {
        let account_store_path = path.to_owned() + ACCOUNTSTORE_DIR;
        AccountStore {
            db: store::Database::init(&account_store_path),
        }
    }

    pub fn create_account(&self, acc: Account) -> Account {
        let account_key = acc.address.as_bytes();
        let acc_bz = acc.encode();
        if self.db.set(account_key, &acc_bz) {
            acc
        } else {
            panic!("Creating account failed.");
        }
    }

    pub fn new_account(&self, addr: &Address) -> Result<Account, MgError> {
        let account_key = addr.as_bytes();
        if let Some(_acc_bz) = self.db.get(account_key) {
            error!("Account existed.");
            Err(MgError::AccountRequestError("Account existed.".to_owned()))
        } else {
            let acc = Account::new(addr, 0, 0, true);
            let created_acc = self.create_account(acc);
            return Ok(created_acc);
        }
    }

    pub fn save_account(&self, acc: Account) -> Result<Account, MgError> {
        let account_key = acc.address.as_bytes();
        if let Some(_) = self.db.get(account_key) {
            let acc_bz = acc.encode();
            if self.db.set(account_key, &acc_bz) {
                return Ok(acc);
            } else {
                error!("Error updating account.");
                Err(MgError::AccountRequestError(
                    "Error updating account.".to_owned(),
                ))
            }
        } else {
            error!("Update failed, account not found.");
            Err(MgError::AccountRequestError(
                "Update failed, account not found.".to_owned(),
            ))
        }
    }

    pub fn get_account(&self, address: &Address) -> Result<Account, MgError> {
        let account_key = address.as_bytes();
        if let Some(acc_bz) = self.db.get(account_key) {
            let acc = Account::decode(acc_bz).unwrap();
            return Ok(acc);
        } else {
            error!("Account not found.");
            Err(MgError::AccountRequestError(
                "Account not found.".to_owned(),
            ))
        }
    }

    pub fn get_account_list(&self) -> Result<HashMap<Vec<u8>, Vec<u8>>, MgError> {
        if let Some(acc_kv) = self.db.get_all() {
            let mut account = HashMap::new();
            for elem in acc_kv {
                let value: Vec<u8> = elem.1.try_into().unwrap();
                let key: Vec<u8> = elem.0.try_into().unwrap();
                account.insert(key, value);
            }

            return Ok(account);
        } else {
            error!("Accounts not found.");
            Err(MgError::AccountRequestError(
                "Accounts not found.".to_owned(),
            ))
        }
    }
}

#[cfg(test)]
#[path = "./account_store_test.rs"]
mod account_store_test;
