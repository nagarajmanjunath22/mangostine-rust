use crate::account_store;
use account_store::AccountStore;
use merkle_tree::trie;
use primitive_types::H256;

//Calculate the app_hash
pub fn calc_state_merkle_root(account_store: AccountStore) -> H256 {
    let acc_list = account_store.get_account_list();
    let app_hash = trie::trie_root_no_extension(acc_list.unwrap());
    return app_hash;
}



#[cfg(test)]
#[path = "./persistence_merkle_test.rs"]
mod persistence_merkle_test;
