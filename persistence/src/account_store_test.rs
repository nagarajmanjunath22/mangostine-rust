mod tests {
    use super::super::*;
    use crypto::keypair::Keypair;
    use types::address::Address;

    #[test]
    fn account_store_test() {
        let account_store: AccountStore = AccountStore::open("test-rocksdb");
        let keypair = Keypair::new();
        let address = Address::from(*keypair.public());

        let mut new_account = account_store.new_account(&address).unwrap();
        assert_eq!(new_account.address, address);
        assert_eq!(new_account.balance, 0);
        assert_eq!(new_account.sequence, 0);

        new_account.balance = new_account.balance + 1;
        new_account.sequence = new_account.sequence + 1;

        let account = account_store.save_account(new_account).unwrap();
        assert_ne!(account.balance, 0);
        assert_ne!(account.sequence, 0);

        let querry_account = account_store.get_account(&address).unwrap();
        assert_eq!(querry_account, account);

        // finish test must drop and destroy.
        drop(account_store);
        KVStore::destroy("test-rocksdb/data/mangostine_account_store");
        KVStore::destroy("test-rocksdb/data");
        KVStore::destroy("test-rocksdb");
    }
}
