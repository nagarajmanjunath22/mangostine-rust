use crate::error::error::MgError;
use crate::store::{self, Database, KVStore};
use log::error;
use primitive_types::H256;
use types::block::{Block, Header};
use types::transaction::Transaction;

pub const BLOCKSTORE_DIR: &str = "/data/mangostine_block_store";
pub const COLUMN_BLOCK: &str = "0";
pub const COLUMN_BLOCK_BODY: &str = "1";
pub const COLUMN_TRANSACTION: &str = "2";

pub struct BlockStore {
    db: KVStore,
}

impl BlockStore {
    // init block store with two column family.
    pub fn open(path: &str) -> Self {
        let mut cols: Vec<&str> = Vec::new();
        cols.push(COLUMN_BLOCK);
        cols.push(COLUMN_BLOCK_BODY);
        cols.push(COLUMN_TRANSACTION);

        let block_store_path = path.to_owned() + BLOCKSTORE_DIR;
        BlockStore {
            db: store::Database::open_cf(&block_store_path, cols),
        }
    }

    // TODO: rename to save_block
    pub fn execute_block(&self, block: &Block) -> Result<(), MgError> {
        // store block hash with block number
        let block_number = block.header.block_number.to_be_bytes();
        if self.db.get_cf(COLUMN_BLOCK, &block_number).is_some() {
            error!("Execute block {:?} failed.", block.hash());
            return Err(MgError::BlockRequestError(
                "Found duplicate block number.".to_owned(),
            ));
        }

        self.db
            .set_cf(COLUMN_BLOCK, &block_number, block.hash().as_bytes());

        // store block body with block hash
        let bh = block.hash();
        let block_key = bh.as_bytes();

        let block_bz = block.encode();
        self.db.set_cf(COLUMN_BLOCK_BODY, block_key, &block_bz);

        // store transactions
        for tx in &block.transactions {
            let tx_hash = tx.hash();
            let tx_key = tx_hash.as_bytes();

            let tx_bz = tx.encode();
            let bz: &[u8] = &tx_bz;

            self.db.set_cf(COLUMN_TRANSACTION, tx_key, bz);
        }

        Ok(())
    }

    pub fn get_block_header(
        &self,
        block_number: Option<u64>,
        block_hash: Option<H256>,
    ) -> Result<Header, MgError> {
        if block_number.is_some() {
            let bn = block_number.unwrap().to_be_bytes();
            let bh = self.db.get_cf(COLUMN_BLOCK, &bn).unwrap();

            let b = self.db.get_cf(COLUMN_BLOCK_BODY, &bh).unwrap();
            let block = Block::decode(b)?;
            return Ok(block.header);
        }
        if block_hash.is_some() {
            let bh = block_hash.unwrap();
            let bh_key = bh.as_bytes();
            let b = self.db.get_cf(COLUMN_BLOCK_BODY, &bh_key).unwrap();
            let block = Block::decode(b)?;
            return Ok(block.header);
        }
        error!("Get Block Header failed.");
        Err(MgError::BlockRequestError(
            "Get Block Header failed.".to_owned(),
        ))
    }

    pub fn get_block(
        &self,
        block_number: Option<u64>,
        block_hash: Option<H256>,
    ) -> Result<Block, MgError> {
        if block_number.is_some() {
            let number = block_number.unwrap();
            let bn = number.to_be_bytes();
            let bh = self.db.get_cf(COLUMN_BLOCK, &bn).unwrap();

            let b = self.db.get_cf(COLUMN_BLOCK_BODY, &bh).unwrap();
            let block = Block::decode(b)?;
            return Ok(block);
        }
        if block_hash.is_some() {
            let bh = block_hash.unwrap();
            let bh_key = bh.as_bytes();
            let b = self.db.get_cf(COLUMN_BLOCK_BODY, &bh_key).unwrap();
            let block = Block::decode(b)?;
            return Ok(block);
        }

        error!("Get Block failed.");
        Err(MgError::BlockRequestError("Get Block failed.".to_owned()))
    }

    pub fn get_transaction(&self, tx_hash: H256) -> Result<Transaction, MgError> {
        let tx_key = tx_hash.as_bytes();
        let tx_bz = self.db.get_cf(COLUMN_TRANSACTION, tx_key).unwrap();
        if !tx_bz.is_empty() {
            let tx = Transaction::decode(tx_bz)?;
            return Ok(tx);
        }

        error!("Transaction not found.");
        Err(MgError::BlockRequestError(
            "Transaction not found.".to_owned(),
        ))
    }
}

#[cfg(test)]
#[path = "./block_store_test.rs"]
mod block_store_test;
