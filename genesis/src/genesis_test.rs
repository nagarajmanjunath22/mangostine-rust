#[cfg(test)]
mod account_test {

    use super::super::*;
    use crypto::keypair::Keypair;
    use std::convert::{From, TryFrom};
    use types::address::Address;
    use Genesis;

    #[test]
    fn genesis_test_by_default() {
        let mut genesis = Genesis::default();

        for _ in 1..5 {
            let (kp, _mnemonic) = Keypair::generate_with_seed();
            let addr = Address::from(*kp.public());
            let acc = Account::new(&addr, 150, 0, false);
            genesis.accounts.push(acc);
        }

        let rs_serialize = Genesis::from(genesis.clone()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(
            rs_serialize.clone(),
            Genesis::try_from(rs_serialize).unwrap()
        );
        assert_eq!(genesis.app_hash.is_zero(), false);
    }

    #[test]
    fn genesis_test_init() {
        let genesis_default = Genesis::default();

        let genesis_time = genesis_default.genesis_time();
        let block_number = genesis_default.block_number;
        let chain_id = genesis_default.chain_id;
        let validators = vec![];
        let accounts = vec![];
        let app_hash = genesis_default.app_hash;
        let consensus_params = genesis_default.consensus_params;

        let mut genesis_init = Genesis::new(
            genesis_time,
            block_number,
            chain_id,
            validators,
            accounts,
            app_hash,
            consensus_params,
        );

        for _ in 1..10 {
            let (kp, _mnemonic) = Keypair::generate_with_seed();
            let addr = Address::from(*kp.public());
            let acc = Account::new(&addr, 150, 0, false);
            genesis_init.accounts.push(acc);
        }

        let rs_serialize = Genesis::from(genesis_init.clone()); // Testing using 'serde_cbor::to_vec' & 'serde_cbor::from_slice'
        assert_eq!(
            rs_serialize.clone(),
            Genesis::try_from(rs_serialize).unwrap()
        );
        assert_eq!(genesis_init.app_hash.is_zero(), false);
    }
}
