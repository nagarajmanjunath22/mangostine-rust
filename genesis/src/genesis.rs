use chrono::{DateTime, NaiveDateTime, Utc};
use primitive_types::{H256, U256};
use types::account::Account;
use types::UnixTimestamp;
use serde::{Serialize, Deserialize};

/// Genesis data
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Genesis {
    /// Time of genesis
    pub genesis_time: UnixTimestamp,

    //Initial Height
    pub block_number: U256,
    /// Accounts
    pub accounts: Vec<Account>,

    /// Chain ID
    pub chain_id: String,

    /// Consensus parameters
    pub consensus_params: String,
    /// Validators
    pub validators: Vec<u8>,

    /// App hash
    pub app_hash: H256,
}

impl Default for Genesis {
    fn default() -> Self {
        let time: DateTime<Utc> = DateTime::from(Utc::now());
        Self {
            genesis_time: time.timestamp() as u64,
            block_number: U256::zero(),
            app_hash: H256::random(),
            chain_id: "mangostine".to_owned(),
            validators: vec![],
            accounts: vec![],
            consensus_params: "consensus".to_owned(),
        }
    }
}

impl Genesis {
    pub fn new(
        genesis_time: DateTime<Utc>,
        block_number: U256,
        chain_id: String,
        validators: Vec<u8>,
        accounts: Vec<Account>,
        app_hash: H256,
        consensus_params: String,
    ) -> Genesis {
        Genesis {
            genesis_time: genesis_time.timestamp() as u64,
            chain_id: chain_id,
            validators: validators,
            accounts: accounts,
            app_hash: app_hash,
            consensus_params: consensus_params,
            block_number: block_number,
        }
    }

    //create the genesis time
    pub fn genesis_time(&self) -> DateTime<Utc> {
        let native = NaiveDateTime::from_timestamp(self.genesis_time as i64, 0);
        DateTime::from_utc(native, Utc)
    }

    //get the Application Hash
    pub fn app_hash(&self) -> H256 {
        self.app_hash
    }
}


#[cfg(test)]
#[path = "./genesis_test.rs"]
mod genesis_test;
