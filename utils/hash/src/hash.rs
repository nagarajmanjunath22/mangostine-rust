use blake3;
use primitive_types::H256;
use std::convert::Into;

pub fn hashing<T: Into<Vec<u8>>>(item: T) -> H256 {
    let bytes: Vec<u8> = item.into();

    let mut hasher = blake3::Hasher::new();
    hasher.update(&bytes);
    let mut hash_output = [0; 32];
    let mut output_reader = hasher.finalize_xof();
    output_reader.fill(&mut hash_output);
    H256(hash_output)
}

#[cfg(test)]
#[path = "./hash_test.rs"]
mod hash_test;
